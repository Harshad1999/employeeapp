import {NavigationContainer} from '@react-navigation/native';
import React, {useRef, useState, useEffect} from 'react';
import {AppState, View, Text, TouchableOpacity, Alert} from 'react-native';
import {Provider} from 'react-redux';
import configStore from './src/redux/Store';
import Auth from './src/AssigmentWeek/Navigator/Auth';
import AuthHome from './src/authHome/AuthHome';
import messaging from '@react-native-firebase/messaging';
import PinCode from './src/Pincode/PinCode';
import PINCode, {
  hasUserSetPinCode,
  resetPinCodeInternalStates,
  deleteUserPinCode,
} from '@haskkor/react-native-pincode';

export default function App() {
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  const [showPinLock, SetshowPinLock] = useState(false);
  const [PINCodeStatus, setPINCodeStatus] = useState('choose');
  let store = configStore();

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        console.log('App has come to the foreground!');
      }

      appState.current = nextAppState;
      setAppStateVisible(appState.current);
      console.log('AppState', appState.current);
      if (appStateVisible === 'active') {
        showEnterPinLock();
      }
    });

    return () => {
      subscription.remove();
    };
  }, []);

  // return null;
  // useEffect(() => {
  //   requestUserPermission();
  //   messaging().setBackgroundMessageHandler(async remoteMessage => {
  //     console.log('Message handled in the background!', remoteMessage);
  //   });
  // }, []);

  // async function requestUserPermission() {
  //   const authStatus = await messaging().requestPermission();
  //   const enabled =
  //     authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
  //     authStatus === messaging.AuthorizationStatus.PROVISIONAL;
  //   if (enabled) {
  //     console.log('Authorization status:', authStatus);
  //     let fcmToken = await messaging().getToken();
  //     console.log(fcmToken);
  //   }
  // }

  const finishProcess = async () => {
    const hasPin = await hasUserSetPinCode();
    if (hasPin) {
      // Alert.alert(null, 'You have successfully set/entered your pin.', [
      //   {
      //     title: 'Ok',
      //     onPress: () => {
      //       // do nothing
      //     },
      //   },
      // ]);
      //   this.setState({showPinLock: false});
      SetshowPinLock(false);
    }
  };

  const showChoosePinLock = () => {
    // this.setState({PINCodeStatus: 'choose', showPinLock: true});
    setPINCodeStatus('choose');
    SetshowPinLock(true);
  };
  const showEnterPinLock = async () => {
    const hasPin = await hasUserSetPinCode();
    if (hasPin) {
      //   this.setState({PINCodeStatus: 'enter', showPinLock: true});
      setPINCodeStatus('enter');
      SetshowPinLock(true);
    } else {
      Alert.alert(null, 'You have not set your pin.', [
        {
          title: 'Ok',
          onPress: () => {
            showChoosePinLock();
            // do nothing
          },
        },
      ]);
    }
  };
  const clearPin = async () => {
    await deleteUserPinCode();
    await resetPinCodeInternalStates();
    Alert.alert(null, 'You have cleared your pin.', [
      {
        title: 'Ok',
        onPress: () => {
          showChoosePinLock();
          // do nothing
        },
      },
    ]);
  };

  return (
    <Provider store={store}>
      {/* <View style={{flex: 1}}>
        <EmpForm />
        <CounterApp />
      </View> */}
      {/* <NavigationContainer>
        <AuthHome />
      </NavigationContainer> */}
      <NavigationContainer>
        {showPinLock && (
          <PINCode
            status={PINCodeStatus}
            finishProcess={() => finishProcess()}
            touchIDDisabled={true}
          />
        )}
        {!showPinLock && <Auth />}
      </NavigationContainer>

      <View>
        <TouchableOpacity onPress={() => clearPin()}>
          <Text style={{fontSize: 20, color: 'black'}}>Clear Pin</Text>
        </TouchableOpacity>
      </View>
      {/* <NavigationContainer>
        <PinCode />
      </NavigationContainer> */}
    </Provider>
  );
}
