import {View, Text, StyleSheet} from 'react-native';
import React, {useState, useEffect} from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';

const Date_Card = props => {
  const [bgColor, setBgColor] = useState('#3dc269');

  useEffect(() => {
    if (props.presence === 'Leave') {
      setBgColor('red');
    }
  }, [bgColor]);

  return (
    <View style={[styles.cardview, {backgroundColor: bgColor}]}>
      <View style={{flex: 0.7, borderRadius: 300}}>
        <Text
          style={{
            fontSize: 25,
            alignSelf: 'center',
            color: 'white',
          }}>
          {props.day}
        </Text>
      </View>
      <View style={{flex: 0.3, justifyContent: 'center', alignSelf: 'center'}}>
        <Text
          style={{
            fontSize: 18,
            alignSelf: 'center',
            color: 'white',
          }}>
          {props.month}
        </Text>

        <Text style={{fontSize: 25, color: 'white'}}>{props.presence}</Text>
      </View>
      {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Feather
          name="trash"
          size={25}
          style={{color: 'white', marginTop: 20}}
          onPress={props.OnPressTrash}
        />
      </View> */}
    </View>
  );
};

const styles = StyleSheet.create({
  cardview: {
    flex: 0.35,
    padding: 5,
    margin: 5,
    borderRadius: 25,
    borderColor: '#808B96',
    borderWidth: 1,
  },
});

export default Date_Card;
