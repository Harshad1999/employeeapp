import {ScrollView, Image, View, StyleSheet, Text} from 'react-native';
import React from 'react';
import ForecastCard from './ForecastCard';
import moment from 'moment-timezone';

const CurrentTempEl = ({data}) => {
  if (data && data.weather) {
    const img = {
      uri:
        'http://openweathermap.org/img/wn/' + data.weather[0].icon + '@4x.png',
    };
    return (
      <View style={styles.currentTempContainer}>
        <Image source={img} style={styles.image} />
        <View style={styles.otherContainer}>
          <Text style={styles.day}>
            {moment(data.dt * 1000).format('dddd')}
          </Text>
          <Text style={styles.temp}>Night - {data.temp.night}&#176;C</Text>
          <Text style={styles.temp}>Day - {data.temp.day}&#176;C</Text>
        </View>
      </View>
    );
  } else {
    return <View></View>;
  }
};

export default function Footer_Card({weatherData}) {
  return (
    <ScrollView horizontal={true} style={styles.scrollView}>
      <CurrentTempEl
        data={weatherData && weatherData.length > 0 ? weatherData[0] : {}}
      />
      <ForecastCard data={weatherData} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    padding: 30,
  },
  image: {
    width: 150,
    height: 150,
  },

  currentTempContainer: {
    flexDirection: 'row',
    backgroundColor: '#00000099',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#eee',
    borderWidth: 2,
    // padding: 15,
  },
  day: {
    fontSize: 30,
    color: 'orange',
    backgroundColor: '#3c3c44',
    // padding: 10,
    textAlign: 'center',
    borderRadius: 25,
    padding: 10,
    marginBottom: 15,
  },
  temp: {
    fontSize: 16,
    color: 'white',
    // fontWeight: '100',
    textAlign: 'center',
  },
  otherContainer: {
    marginRight: 40,
    
  },
});
