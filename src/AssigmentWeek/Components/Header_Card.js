import {View, Text, StyleSheet} from 'react-native';
import React, {useState, useEffect} from 'react';
import moment from 'moment-timezone';

const WeatherItem = ({title, value, unit}) => {
  return (
    <View style={styles.weatherItem}>
      <Text style={styles.wTitle}>{title}</Text>
      <Text style={styles.wTitle}>
        {value}
        {unit}
      </Text>
    </View>
  );
};

export default function Header_Card({current, lat, lon, timezone}) {
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');

  const days = [
    'sunday',
    'Monday',
    'Tuesday',
    'wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  useEffect(() => {
    setInterval(() => {
      const time = new Date();
      const month = time.getMonth();
      const date = time.getDate();
      const day = time.getDay();
      const hour = time.getHours();
      const hoursIn12HrFormat = hour >= 13 ? hour % 12 : hour;
      const minutes = time.getMinutes();
      const ampm = hour >= 12 ? 'pm' : 'am';

      setTime(
        (hoursIn12HrFormat < 10 ? '0' + hoursIn12HrFormat : hoursIn12HrFormat) +
          ':' +
          (minutes < 10 ? '0' + minutes : minutes) +
          ' ' +
          ampm,
      );
      setDate(days[day] + ', ' + date + ' ' + months[month]);
    }, 1000);
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <View>
          <Text style={styles.heading}>{time}</Text>
        </View>
        <View>
          <Text style={styles.subheading}>{date}</Text>
        </View>
        <View style={styles.rightAlign}>
          <Text style={styles.timezone}>{timezone}</Text>
          <Text style={styles.latLong}>
            {lat}N {lon}E
          </Text>
        </View>
        <View style={styles.weatherItemBox}>
          <WeatherItem
            title="Humidity"
            value={current ? current.humidity : ''}
            unit="%"
          />
          <WeatherItem
            title="Pressure"
            value={current ? current.pressure : ''}
            unit="hPA"
          />
          <WeatherItem
            title="Sunrise"
            value={
              current
                ? moment.tz(current.sunrise, timezone).format('HH:mm')
                : ''
            }
            unit="am"
          />
          <WeatherItem
            title="Sunset"
            value={
              current ? moment.tz(current.sunset, timezone).format('HH:mm') : ''
            }
            unit="pm"
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    // flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    // marginTop: 20,
    // backgroundColor: '#18181b66',
  },
  heading: {
    fontSize: 65,
    color: 'white',
    fontWeight: '400',
  },
  subheading: {
    fontSize: 30,
    color: '#eee',
    fontWeight: '400',
  },
  rightAlign: {
    textAlign: 'right',
    marginTop: 20,
    marginBottom: 20,
  },
  timezone: {
    fontSize: 25,
    fontWeight: '700',
    color: 'white',
  },

  latLong: {
    fontSize: 20,
    color: 'white',
    fontWeight: '400',
  },
  weatherItemBox: {
    backgroundColor: '#18181b66',
    // backgroundColor: '#000000c0',
    borderRadius: 10,
    margin: 15,
    padding: 15,
    borderWidth: 1.5,
    borderColor: '#fff',
  },
  weatherItem: {flexDirection: 'row', justifyContent: 'space-between'},
  wTitle: {fontSize: 18, color: '#eee', fontWeight: '400'},
});
