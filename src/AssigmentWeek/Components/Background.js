import {View, Text} from 'react-native';
import React from 'react';

export default function Background() {
  const TASK_NAME = 'NOTIFICATIONS';

  TaskManager.defineTask(TASK_NAME, () => {
    try {
      // fetch data here...
      const receivedNewData = 'Simulated fetch ' + Math.random();
      console.log('My task ', receivedNewData);
      if (notUndefinedAndNull(receivedNewData)) {
        sendNotificationImmediately();
      }
      return receivedNewData
        ? BackgroundFetch.Result.NewData
        : BackgroundFetch.Result.NoData;
    } catch (err) {
      return BackgroundFetch.Result.Failed;
    }
  });

  async function sendNotificationImmediately() {
    Notifications.setNotificationHandler({
      handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: false,
      }),
    });

    Notifications.scheduleNotificationAsync({
      content: {
        title: 'Look at that notification',
        body: "I'm so proud of myself!",
        sound: true,
        vibrate: true,
      },
      trigger: null,
    });
  }

  async function checkBackgroundTaskStatus() {
    let status = await BackgroundFetch.getStatusAsync();
    switch (status) {
      case BackgroundFetch.Status.Restricted:
      case BackgroundFetch.Status.Denied:
        console.log('return');
        return;

      default: {
        let tasks = await TaskManager.getRegisteredTasksAsync();
        checkTasks(tasks);
      }
    }
  }

  function checkTasks(tasks) {
    if (!undefinedOrZero(tasks)) {
      for (let i = 0; i < tasks.length; i++) {
        let obj = tasks[i];
        if (obj.taskName !== 'NOTIFICATIONS') {
          registerBackgroundTask();
          return;
        }
      }
    } else {
      registerBackgroundTask();
    }
  }

  async function registerBackgroundTask() {
    try {
      await BackgroundFetch.registerTaskAsync(TASK_NAME, {
        minimumInterval: 3, // seconds,
      });
      console.log('Task registered');
    } catch (err) {
      console.log('Task Register failed:', err);
    }
  }

  return (
    <View>
      <Text>Background</Text>
    </View>
  );
}
