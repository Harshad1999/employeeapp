import {View, Text, StyleSheet, Image, Touchable} from 'react-native';
import React, {useState, useEffect} from 'react';

export default function Json_Card(props) {
  const [colour, setColour] = useState('green');
  useEffect(() => {
    if (props.completed === true) {
      setColour('red');
      return;
    } else setColour('green');
  }, []);

  return (
    <View style={style.container}>
      <View style={{flex: 1, flexDirection: 'row', padding: 10}}>
        <Text style={style.idText}>{props.id}</Text>
        <Text style={[style.nameText, {color: colour}]}>{props.title}</Text>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
    padding: 10,
    height: '10%',
    backgroundColor: 'pink',
    borderRadius: 15,
  },

  nameText: {
    fontSize: 20,
  },
  image: {
    width: '100%',
    height: 110,

    borderRadius: 50,

    resizeMode: 'cover',
  },
  detailText: {
    fontSize: 17,
  },

  imgView: {
    flex: 0.4,
    flexDirection: 'row',
    margin: 10,
  },
  CardView: {
    flex: 0.7,
    marginLeft: 10,
  },
  idText: {fontSize: 20, marginRight: 10, color: '#000'},
});
