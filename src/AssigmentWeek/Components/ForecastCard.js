import {View, Text, Image, ScrollView, StyleSheet} from 'react-native';
import React from 'react';
import moment from 'moment-timezone';

export default function ForecastCard({data}) {
  return (
    <ScrollView horizontal={true}  style={styles.scrollview}>
      {data && data.length > 0 ? (
        data.map(
          (data, index) =>
            index !== 0 && <FutureForecastItem forecastItem={data} />,
        )
      ) : (
        <View></View>
      )}
    </ScrollView>
  );
}

function FutureForecastItem({forecastItem}) {
  const img = {
    uri:
      'http://openweathermap.org/img/wn/' +
      forecastItem.weather[0].icon +
      '@4x.png',
  };
  return (
    <View style={styles.currentTempContainer}>
      <Image source={img} style={styles.image} />
      <View style={styles.otherContainer}>
        <Text style={styles.day}>
          {moment(forecastItem.dt * 1000).format('ddd')}
        </Text>
        <Text style={styles.temp}>
          Night - {forecastItem.temp.night}&#176;C
        </Text>
        <Text style={styles.temp}>Day - {forecastItem.temp.day}&#176;C</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  scrollview: {
    flex: 0.5,
    // padding: 30,
    marginLeft: 30,
    marginRight: 30,
  },
  image: {
    width: 150,
    height: 150,
  },

  currentTempContainer: {
    flexDirection: 'row',
    margin: 10,
    backgroundColor: '#00000044',
    // backgroundColor: '#00000099',

    padding: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#eee',
    borderWidth: 2,
  },
  day: {
    fontSize: 25,
    color: 'white',
    backgroundColor: '#3c3c44',
    padding: 10,
    textAlign: 'center',
    borderRadius: 50,
    marginBottom: 15,
  },
  temp: {
    fontSize: 16,
    color: 'white',
    // fontWeight: '100',
    textAlign: 'center',
  },
  otherContainer: {
    paddingRight: 40,
  },
});
