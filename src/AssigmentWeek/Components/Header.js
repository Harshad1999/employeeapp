import {View, Text, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import {removeEmpUser} from '../../utils/Storage';

export default function Header(props) {
  let navigation = useNavigation();

  async function clearSession() {
    let rm = await removeEmpUser('key');
    console.log(rm);
    if (rm) {
      navigation.navigate('Login');
    }
  }
  function Logout() {
    Alert.alert('Logout', 'Are you sure want to Logout', [
      {
        text: 'Cancel',
        onPress: () => navigation.navigate('Drawer'),
        style: 'cancel',
      },
      {
        text: 'Ok',
        onPress: () => {
          // clearSession();
          navigation.navigate('Login');
        },
      },
    ]);
  }

  return (
    <View style={styles.wrapper}>
      <View style={styles.headerStyle}>
        <TouchableOpacity
          onPress={() => props.navigation.toggleDrawer()}
          style={{justifyContent: 'center'}}>
          <Feather name="menu" style={{fontSize: 30, color: 'black'}} />
        </TouchableOpacity>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            width: '75%',
          }}>
          <Text
            style={{
              fontSize: 25,
              alignSelf: 'center',
              color: 'black',
            }}>
            Employee App
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => {
            Logout();
          }}
          style={{justifyContent: 'center'}}>
          <Feather name="log-out" style={{fontSize: 30, color: 'black'}} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 60,
    elevation: 10,
    justifyContent: 'center',
  },
  headerStyle: {
    height: 60,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
  },
});
