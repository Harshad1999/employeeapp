import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';

export default function EmpView(props) {
  return (
    <View style={style.container}>
      <TouchableOpacity onPress={props.onPressCard}>
        <View style={{flex: 1}}>
          <Text style={style.idText}>{props.id}</Text>
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={style.imgView}>
            <Image style={style.image} source={{uri: props.profImage}} />
          </View>
          <View style={style.CardView}>
            <Text style={style.nameText}>{props.name}</Text>
            <Text style={style.detailText}>{props.email}</Text>
            <Text style={style.detailText}>{props.designation}</Text>
          </View>
        </View>
      </TouchableOpacity>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Feather
          name="trash"
          size={25}
          style={{color: 'white', marginTop: 20}}
          onPress={props.OnPressTrash}
        />
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
    padding: 10,
    height: '10%',
    backgroundColor: '#629d88',
    borderRadius: 15,
  },

  nameText: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#fff',
  },
  image: {
    width: '100%',
    height: 110,

    borderRadius: 50,

    resizeMode: 'cover',
  },
  detailText: {
    fontSize: 17,
    // fontWeight:'500',
    color: '#D8E3E7',
  },

  imgView: {
    flex: 0.4,
    flexDirection: 'row',
    margin: 10,
  },
  CardView: {
    flex: 0.7,
    marginLeft: 10,
  },
  idText: {fontSize: 20, color: '#fff'},
});
