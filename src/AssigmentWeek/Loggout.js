import {View, Text} from 'react-native';
import React, {useEffect} from 'react';
import {removeEmpUser} from '../utils/Storage';
import {useNavigation} from '@react-navigation/native';

export default function Loggout() {
  let navigation = useNavigation();

  useEffect(() => {
    clearSession();
  }, []);

  async function clearSession() {
    let rm = await removeEmpUser('key');
    console.log(rm);
    if (rm) {
      navigation.navigate('Login');
    }
  }

  return (
    <View>
      <Text>Logout</Text>
    </View>
  );
}
