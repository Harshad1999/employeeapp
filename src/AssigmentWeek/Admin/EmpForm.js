import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  Alert,
  ScrollView,
  TextInput,
  ToastAndroid,
} from 'react-native';
import {RadioButton} from 'react-native-paper';
import Feather from 'react-native-vector-icons/dist/Feather';

import React, {useEffect, useState} from 'react';
import ImagePicker from 'react-native-image-crop-picker';
import {empty, valAlpha, valEmail, valNum} from '../../utils/Validate';
import {getEmpAsync, setEmpAsync} from '../../utils/Storage';
import useTextInput from '../customHooks/useTextInput';
import {inputValidation, lengthValidation} from '../../utils/ValidationSchema';

export default function EmpForm() {
  const [checked, setChecked] = useState('no');

  let employeeIdInput = useTextInput(
    '',
    'numeric',
    '101',
    inputValidation(true, 'Id cannot be blank'),
  );
  let nameInput = useTextInput(
    '',
    'text',
    'eg:john',
    lengthValidation(true, 'length cannot be more than 20 char '),
  );
  let emailInput = useTextInput(
    '',
    'text',
    'eg:john@gmail.com',
    inputValidation(true, 'Email cannot be blank'),
  );
  let designationInput = useTextInput(
    '',
    'text',
    'eg:SDE',
    inputValidation(true, 'Designation cannot be blank'),
  );
  let projectValueInput = useTextInput(
    '',
    'text',
    'eg:Zomato',
    inputValidation(true, 'Project name cannot be blank'),
  );

  // const [empIdValue, setEmpIdValue] = useState({
  //   value: '',
  //   error: '',
  // });
  // const [empNameValue, setEmpNameValue] = useState({
  //   value: '',
  //   error: '',
  // });
  // const [empEmailValue, setEmpEmailValue] = useState({
  //   value: '',
  //   error: '',
  // });
  // const [empDesignationValue, setEmpDesignationValue] = useState({
  //   value: '',
  //   error: '',
  // });
  // const [empProjectValue, setEmpProjectValue] = useState({
  //   value: '',
  //   error: '',
  // });

  const [empData, setEmpData] = useState([]);
  const [image, setImage] = useState('');

  useEffect(() => {
    employeeIdInput.onChangeText('');
  }, []);

  function clearValue() {
    setEmpIdValue('');
    setEmpNameValue('');
    setEmpEmailValue('');
    setEmpDesignationValue('');
    setEmpProjectValue('');
    setImage('');
  }

  const showToastError = () => {
    ToastAndroid.show('Nothing was Added', ToastAndroid.SHORT);
  };
  const showToastSuccess = () => {
    ToastAndroid.show('Data Added Successfully', ToastAndroid.SHORT);
  };
  async function OnPressAddToList() {
    console.log(employeeIdInput.value, nameInput.value);
    let isValid = formValid();
    if (isValid) {
      let newEmpData = {
        id: empIdValue.value,
        name: empNameValue.value,
        email: empEmailValue.value,
        designation: empDesignationValue.value,
        proName: empProjectValue.value,
        proDeployed: checked,
        profImage: image
          ? image
          : 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png',
      };

      let oldData = await getEmpAsync('employeeKey');
      console.log(oldData);

      let temp = [...empData];
      if (empty(oldData)) {
        oldData = [];
      }
      oldData.push(newEmpData);
      setEmpData(temp);
      setEmpAsync('employeeKey', oldData);
      clearValue();
      showToastSuccess();
    } else showToastError();
  }

  function formValid() {
    let valid = true;
    let error = 'Field Cannot be Empty';

    if (empty(employeeIdInput.value)) {
      valid = false;
      setEmpIdValue({error: error});
    }

    if (empty(empNameValue.value)) {
      valid = false;
      setEmpNameValue({error: error});
    }

    if (empty(empEmailValue.value)) {
      valid = false;
      setEmpEmailValue({error: error});
    }
    if (empty(empDesignationValue.value)) {
      valid = false;
      setEmpDesignationValue({error: error});
    }

    if (empty(empProjectValue.value)) {
      valid = false;
      setEmpProjectValue({error: error});
    }

    return valid;
  }

  function OnChangeIdValue(v) {
    let error = '';
    let value = v;
    if (!valNum(v)) {
      error = 'Enter valid Id';
    }
    setEmpIdValue({...empIdValue, value: value, error: error});
  }
  function OnChangeNameValue(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Name';
    }
    if (value.length > 31) {
      error = 'Characters should be less than 30';
    }
    setEmpNameValue({...empNameValue, value: value, error: error});
  }
  function OnChangeEmailText(v) {
    let error = '';
    let value = v;
    if (!valEmail(v)) {
      error = 'Enter valid Email';
    }
    setEmpEmailValue({...empEmailValue, value: value, error: error});
  }
  function OnChangeDesignationValue(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Designation';
    }
    if (value.length > 21) {
      error = 'Characters should be less than 20';
    }

    setEmpDesignationValue({
      ...empDesignationValue,
      value: value,
      error: error,
    });
  }
  function OnChangeProjectText(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Project Name';
    }
    setEmpProjectValue({...empProjectValue, value: value, error: error});
  }

  async function OpenCamera() {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      setImage(image.path);
    });
  }

  async function pickImage() {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      setImage(image.path);
    });
  }

  function OpenChooseAlert() {
    Alert.alert('Select Option', 'Set Employee Photo', [
      {
        text: 'cancel',
        onPress: () => console.log('OK Pressed'),
      },
      {
        text: 'Open Camera',
        onPress: () => {
          OpenCamera();
        },
        // style: 'cancel',
      },
      {
        text: 'Pick Image',
        onPress: () => {
          pickImage();
        },
      },
    ]);
  }

  return (
    <View style={styles.container}>
      <View style={{flex: 0.2, alignItems: 'center'}}>
        <TouchableOpacity onPress={OpenChooseAlert}>
          <Image
            source={{
              uri: image
                ? image
                : 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png',
            }}
            style={styles.profileImg}
          />
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.main}>
        <Text style={styles.text_main}>Employee Id : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              placeholderTextColor="grey"
              {...employeeIdInput}
            />
            {!empty(employeeIdInput.valid) && (
              <Text style={styles.errorMsg}>
                {employeeIdInput.errorMessage}
              </Text>
            )}
          </View>
        </View>
        <Text style={styles.text_main}>Name : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              autoCapitalize="none"
              placeholderTextColor="grey"
              {...nameInput}
            />
            {!empty(nameInput.valid) && (
              <Text style={styles.errorMsg}>{nameInput.errorMessage}</Text>
            )}
          </View>
        </View>
        <Text style={styles.text_main}>Email : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              autoCapitalize="none"
              placeholderTextColor="grey"
              {...emailInput}
            />
            {!empty(emailInput.valid) && (
              <Text style={styles.errorMsg}>{emailInput.errorMessage}</Text>
            )}
          </View>
        </View>
        <Text style={styles.text_main}>Designation: </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              autoCapitalize="none"
              placeholderTextColor="grey"
              {...designationInput}
            />
            {!empty(designationInput.valid) && (
              <Text style={styles.errorMsg}>
                {designationInput.errorMessage}
              </Text>
            )}
          </View>
        </View>
        <Text style={styles.text_main}>Project Name : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              autoCapitalize="none"
              placeholderTextColor="grey"
              {...projectValueInput}
            />
            {!empty(projectValueInput.valid) && (
              <Text style={styles.errorMsg}>
                {projectValueInput.errorMessage}
              </Text>
            )}
          </View>
        </View>
        <View style={{flexDirection: 'row', marginTop: 15}}>
          <Text style={styles.text_main}>Project Deployed : </Text>

          <RadioButton
            value="yes"
            status={checked === 'yes' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('yes')}
            uncheckedColor="black"
            color="black"
          />
          <Text style={styles.text_main}>Yes </Text>

          <RadioButton
            value="no"
            status={checked === 'no' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('no')}
            uncheckedColor="black"
            color="black"
          />
          <Text style={styles.text_main}>No </Text>
        </View>

        <View style={styles.button}>
          <TouchableOpacity style={styles.save} onPress={OnPressAddToList}>
            <Text style={{fontSize: 19, fontWeight: 'bold', color: 'white'}}>
              Add Employee
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

function connectToStore(store) {
  return {
    loginList: store.cartReducer.loginList,
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#629d88',
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  profileImg: {
    // margin: '3%',
    height: 100,
    width: 100,
    borderRadius: 80,
    marginTop: 2,
  },
  main: {
    flex: 0.4,
    width: '95%',
    height: '100%',
    // backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingHorizontal: 20,
    paddingVertical: 15,
    // backgroundColor: '#06F981',
    backgroundColor: '#fff',
  },
  input: {
    // width:'100%',
    // height: 45,
    // margin: 10,
    borderWidth: 0.5,
    padding: 10,
    // backgroundColor: 'grey',
    borderRadius: 10,
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 13,
  },
  textInput: {
    marginTop: -15,
    fontSize: 13,
    color: 'black',
    borderBottomWidth: 1,
    width: 300,
    borderBottomColor: 'black',
  },
  text_main: {
    fontSize: 15,
    color: 'black',
    marginTop: 10,
  },
  input: {
    flexDirection: 'row',
    marginTop: 5,
  },
  save: {
    width: '50%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    // borderColor: 'black',
    backgroundColor: '#629d88',
    marginBottom: 15,
    paddingRight: 20,
    paddingLeft: 20,
  },
  button: {
    alignItems: 'center',
    marginTop: '20%',
  },
});
