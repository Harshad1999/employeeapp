import {
  View,
  TextInput,
  Text,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import Feather from 'react-native-vector-icons/dist/Feather';
import {FlatList} from 'react-native-gesture-handler';
import _ from 'lodash';
import {empty, isArrEmpty} from '../../utils/Validate';
import {getEmpAsync, removeEmpAsync, setEmpAsync} from '../../utils/Storage';
import EmpView from '../Components/EmpView';
import Popup from '../Components/Popup';

export default function EmpList() {
  const [empData, setEmpData] = useState([]);
  const [search, setsearch] = useState('');

  const [action, setAction] = useState(false);
  const [filter, setFilter] = useState('name');

  let navigation = useNavigation();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      async function gettingData() {
        let employeeData = await getEmpAsync('employeeKey');
        console.log('get wala data', employeeData);

        if (!isArrEmpty(employeeData)) {
          setEmpData(employeeData);
        }
      }
      gettingData();
    });

    return () => {
      unsubscribe;
    };
  }, [navigation]);

  function renderEmpData() {
    let newData = [...empData];
    if (!empty(search)) {
      newData = empData.filter(item => {
        if (filter === 'name') {
          const itemData = item.name
            ? item.name.toUpperCase()
            : ''.toUpperCase();
          const textData = search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        }
        if (filter === 'designation') {
          const itemData = item.designation
            ? item.designation.toUpperCase()
            : ''.toUpperCase();
          const textData = search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        }
      });
    }
    return (
      <FlatList
        data={newData}
        keyExtractor={item => item.id}
        renderItem={({item, index}) => {
          return (
            <EmpView
              id={!empty(item.id) ? item.id : 'No Id'}
              name={!empty(item.name) ? item.name : 'No Name'}
              email={!empty(item.email) ? item.email : 'No email'}
              designation={
                !empty(item.designation) ? item.designation : 'No designation'
              }
              profImage={item.profImage}
              OnPressTrash={() => removeObj(item, index)}
              onPressCard={() => console.log('Card Pressed')}
            />
          );
        }}
      />
    );
  }

  function OnchangeSearch(text) {
    setsearch(text);
  }

  function SearchViaDesignation() {
    setFilter('designation');
    setAction(false);
  }

  function SearchViaName() {
    setFilter('name');
    setAction(false);
  }

  function removeObj(item, index) {
    let temp = [...empData];
    temp.splice(index, 1);
    setEmpData(temp);
    setEmpAsync('employeeKey', temp);
  }

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <StatusBar backgroundColor="grey" />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            borderWidth: 2,
            alignItems: 'center',
            flexDirection: 'row',
            padding: 5,
            borderColor: 'black',
            borderRadius: 12,
            height: 50,
            marginEnd: 10,
            marginStart: 10,
            margin: 8,
          }}>
          <Feather
            name="search"
            color="grey"
            size={18}
            style={{marginEnd: 5, marginStart: 5}}
          />
          <TextInput
            style={{
              width: '80%',
              marginRight: 5,
              fontSize: 15,
              color: 'black',
            }}
            placeholder="Search Employee"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={search}
            onChangeText={text => OnchangeSearch(text)}
          />
          <Feather
            name="more-vertical"
            size={25}
            color="black"
            onPress={() => setAction(!action)}
          />
        </View>
      </View>
      {action && (

<Popup/>

        // <View
        //   style={{
        //     width: 130,
        //     height: 80,
        //     backgroundColor: '#fff',
        //     marginLeft: 220,
        //     zIndex: 1,
        //     borderWidth: 2,
        //     marginTop: 40,
        //     borderRadius: 10,
        //     position: 'absolute',
        //     justifyContent: 'space-evenly',
        //   }}>
        //   <TouchableOpacity onPress={SearchViaName}>
        //     <Text
        //       style={{
        //         color: 'black',
        //         justifyContent: 'center',
        //         textAlign: 'center',
        //         fontSize: 18,
        //       }}>
        //       Name
        //     </Text>
        //   </TouchableOpacity>

        //   <TouchableOpacity onPress={SearchViaDesignation}>
        //     <Text
        //       style={{
        //         color: 'black',
        //         justifyContent: 'center',
        //         textAlign: 'center',
        //         fontSize: 18,
        //       }}>
        //       Role
        //     </Text>
        //   </TouchableOpacity>
        // </View>
      )}
      <View style={{flex: 0.9}}>{renderEmpData()}</View>
    </View>
  );
}
