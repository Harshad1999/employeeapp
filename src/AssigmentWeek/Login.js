import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';
import {empty, isArrEmpty, valEmail, valNum} from '../utils/Validate';
import {getEmpUser, setEmpUser} from '../utils/Storage';
import {useNavigation} from '@react-navigation/native';
import {getLoginData} from './LoginData';

export default function Login() {
  let navigation = useNavigation();

  const [showPassword, setShowPassword] = useState(true);
  const [inputUser, setinputUser] = useState('');
  const [inputPass, setinputPass] = useState('');
  const [errorUser, seterrorUser] = useState('');
  const [errorPass, seterrorPass] = useState('');
  const [errorgeneral, seterrorgeneral] = useState('');
  const [logData, setLogData] = useState([]);
  const [access, setAccess] = useState('');
  // useEffect(() => {
  //   sessionCheck();
  // }, []);
  useEffect(() => {
    let userData = getLoginData();
    setLogData(userData);
  }, []);

  // async function sessionCheck() {
  //   if (!empty(logData.password)) {
  //     navigation.navigate('Drawer');
  //   }
  // }

  function OnChangeUserText(text) {
    setinputUser(text);
    seterrorUser('');
    seterrorPass('');
    seterrorgeneral('');
  }
  function OnChangePassText(text) {
    setinputPass(text);
    seterrorUser('');
    seterrorPass('');
    seterrorgeneral('');
  }

  const showToastError = () => {
    ToastAndroid.show('Invalid User', ToastAndroid.SHORT);
  };

  const showToastWelcome = () => {
    ToastAndroid.show('Welcome', ToastAndroid.SHORT);
  };

  async function OnCLickForgot() {
    let getusername = await getEmpUser('key');
    console.log('getuserloginpage', getusername);
    setinputUser(getusername.username);
  }

  function clearValue() {
    // seterrorUser('');
    // seterrorPass('');
    setinputUser('');
    setinputPass('');
    seterrorgeneral('');
  }

  function onPressLogin() {
    let isValidUser = isValid();
    if (isValidUser) {
      let newData = {
        username: inputUser,
        password: inputPass,
      };

      for (let i = 0; i < logData.length; i++) {
        const element = logData[i];
        if (
          newData.username === element.username &&
          newData.password === element.password
        ) {
          newData.access = element.access;
        }
      }

      for (let i = 0; i < logData.length; i++) {
        const element = logData[i];
        if (inputUser === element.username && inputPass === element.password) {
          setEmpUser('key', newData);
          seterrorUser('');
          seterrorPass('');
          clearValue();
          showToastWelcome();

          navigation.navigate('Drawer');
          return;
        } else {
          seterrorgeneral('Invalid Login User or Password');
        }
      }

      // setEmpUser('key', newData);
      // clearValue();
      // navigation.navigate('Drawer');
    }
  }
  function isValid() {
    let valid = true;
    let errorUserMsg = '';
    let errorPassMsg = '';

    if (empty(inputUser)) {
      valid = false;
      errorUserMsg = 'Enter Valid Username';
      seterrorUser(errorUserMsg);
    }
    if (empty(inputPass)) {
      valid = false;
      errorPassMsg = 'Enter valid Password';
      seterrorPass(errorPassMsg);
    }
    if (!valEmail(inputUser)) {
      valid = false;
      errorUserMsg = 'Enter Valid Username';
      seterrorUser(errorUserMsg);
    }

    return valid;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#629d88" />
      <View style={styles.header}>
        <Text style={styles.text_header}>Login!</Text>
      </View>
      <View style={styles.main}>
        <Text style={styles.text_main}>Username</Text>
        <View style={styles.input}>
          <Feather name="user" color="white" size={20} />
          <TextInput
            style={styles.textInput}
            placeholder="Enter Username"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={inputUser}
            onChangeText={OnChangeUserText}
          />
        </View>

        {!empty(errorUser) && (
          <View>
            <Text style={styles.errorMsg}>{errorUser}</Text>
          </View>
        )}

        <Text style={styles.text_main}>Password</Text>
        <View style={styles.input}>
          <Feather name="lock" color="white" size={20} />
          <TextInput
            style={styles.textInput}
            secureTextEntry={showPassword}
            placeholder="Enter Password"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={inputPass}
            onChangeText={OnChangePassText}
          />
          <TouchableOpacity onPress={() => setShowPassword(!showPassword)}>
            {!showPassword && <Feather name="eye-off" color="grey" size={20} />}

            {showPassword && <Feather name="eye" color="grey" size={20} />}
          </TouchableOpacity>
        </View>

        {!empty(errorPass) && (
          <View>
            <Text style={styles.errorMsg}>{errorPass}</Text>
          </View>
        )}

        <TouchableOpacity onPress={OnCLickForgot}>
          <Text style={{color: '#629d88', marginTop: 17, fontSize: 17}}>
            Forgot password?
          </Text>
        </TouchableOpacity>
        <View style={styles.button}>
          {!empty(errorgeneral) && (
            <View>
              <Text style={styles.errorMsg}>{errorgeneral}</Text>
            </View>
          )}
          <TouchableOpacity style={styles.signInBtn} onPress={onPressLogin}>
            <Text style={styles.textSignIn}>Sign In</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#629d88',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  main: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
    backgroundColor: '#424242',
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_main: {
    fontSize: 18,
    color: 'white',
    marginTop: 30,
  },
  input: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    paddingBottom: 5,
  },

  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 10,
    color: 'white',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 15,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signInBtn: {
    width: '60%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#629d88',
    marginTop: 15,
  },
  textSignIn: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
  },
});
