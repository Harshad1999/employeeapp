import {AppState} from 'react-native';
import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Login from '../Login';
import EmpList from '../Admin/EmpList';
import EmpForm from '../Admin/EmpForm';
import Drawer from './Drawer';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function Auth() {
  return (
    <Stack.Navigator>
      {/* <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      /> */}
      <Stack.Screen
        name="Drawer"
        component={Drawer}
        options={{headerShown: false}}
      />
      <Stack.Screen name="EmpList" component={EmpList} />
      <Stack.Screen name="EmpForm" component={EmpForm} />
    </Stack.Navigator>
  );
}
