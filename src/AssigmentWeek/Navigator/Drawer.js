import React, {useState, useEffect} from 'react';
import EmpForm from '../Admin/EmpForm';
import EmpList from '../Admin/EmpList';
import Loggout from '../Loggout';
import History from '../User/History';
import Setting from '../User/Setting';
import Timeline from '../User/Timeline';
import WelcomeMain from '../User/WelcomeMain';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {empty} from '../../utils/Validate';
import Attendance from '../User/Attendance';
import RenderJsonData from '../RenderJsonData';
import Date_Card from '../Components/Date_Card';
import {getEmpUser} from '../../utils/Storage';
import Weather from '../User/Weather';
import Header from '../Components/Header';
import CounterApp from '../CounterApp';
import CounterClass from '../User/CounterClass';

const DrawerNav = createDrawerNavigator();

export default function Drawer(props) {
  const [initialRouter, setInitialRouter] = useState('EmpForm');

  const [userRole, setUerRole] = useState('');
  useEffect(() => {
    async function checkUserRole() {
      let role = await getEmpUser('key');
      console.log('getuser ka data', role.access);
      setUerRole(role.access);
      if (userRole === 'user') {
        setInitialRouter('attendance');
      }
    }
    checkUserRole();
  }, []);

  return (
    <DrawerNav.Navigator
      initialRouteName={initialRouter}
      screenOptions={{
        drawerActiveBackgroundColor: '#629d88',
        drawerActiveTintColor: 'white',
        header: props => <Header {...props} />, //<------ Custom Header with Props
        headerShown: true,
      }}>
      {!empty(userRole) && userRole === 'admin' && (
        <DrawerNav.Screen name="EmpForm" component={EmpForm} />
      )}

      {!empty(userRole) && userRole === 'admin' && (
        <DrawerNav.Screen name="EmpList" component={EmpList} />
      )}
      {!empty(userRole) && userRole === 'admin' && (
        <DrawerNav.Screen name="Setting" component={Setting} />
      )}
      {!empty(userRole) && userRole === 'user' && (
        <DrawerNav.Screen name="attendance" component={Attendance} />
      )}
      {!empty(userRole) && userRole === 'user' && (
        <DrawerNav.Screen name="Timeline" component={Timeline} />
      )}
      <DrawerNav.Screen name="CounterClass" component={CounterClass} />

      <DrawerNav.Screen name="WelcomeMain" component={WelcomeMain} />
      <DrawerNav.Screen name="RenderJsonData" component={RenderJsonData} />
      <DrawerNav.Screen name="JsonCard" component={Date_Card} />
      <DrawerNav.Screen name="weather" component={Weather} />
      <DrawerNav.Screen name="History" component={History} />
      <DrawerNav.Screen
        name="Logout"
        component={Loggout}
        // options={{headerShown: false}}
      />
    </DrawerNav.Navigator>
  );
}
