import React, {useState} from 'react';
import {empty} from '../../utils/Validate';

export default function useTextInput(
  defaultValue,
  keyboardType,
  holderValue,
  validationSchema,
) {
  const [value, setValue] = useState(defaultValue);
  const [valid, setValid] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');

  function handleOnchangeValue(v) {
    let tempError = '';
    let isValid = true;
    if (validationSchema !== undefined && validationSchema !== null) {
      if (validationSchema.required && empty(v)) {
        tempError = validationSchema.message;
        isValid = false;
      }
      if (
        validationSchema.required &&
        'type' in validationSchema &&
        validationSchema.type === 'length'
      ) {
        if (!empty(v) && v.length > 20) {
          tempError = validationSchema.message;
          isValid = false;
        }
      }
    }
    setValue(v);
    setValid(isValid);
    setErrorMessage(tempError);
  }

  return {
    value,
    keyboardType,
    errorMessage,
    valid,
    placeholder: holderValue,
    onChangeText: v => handleOnchangeValue(v),
  };
}
