import {
  View,
  TextInput,
  StatusBar,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import Feather from 'react-native-vector-icons/dist/Feather';
import {empty, isArrEmpty} from '../utils/Validate';
import _ from 'lodash';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import * as empAssAction from '../redux/action/EmpAssAction';
import Json_Card from './Components/Json_Card';

export default function RenderJsonData() {
  const [empData, setEmpData] = useState([]);
  const [search, setsearch] = useState('');

  let navigation = useNavigation();
  let store = useSelector(connectToStore, shallowEqual);
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(empAssAction.getJson());
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      async function gettingData() {
        let employeeData = await store.jsonList;
        if (!isArrEmpty(employeeData)) {
          setEmpData(employeeData);
        }
      }
      gettingData();
    });

    return () => {
      unsubscribe;
    };
  }, [navigation]);

  function renderData() {
    let newData = _.cloneDeep(store.jsonList);
    if (!empty(search)) {
      newData = store.jsonList.filter(item => {
        const itemData = item.title
          ? item.title.toUpperCase()
          : ''.toUpperCase();
        const textData = search.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
    }
    return (
      <FlatList
        data={newData}
        keyExtractor={item => item.id}
        renderItem={({item, index}) => {
          return (
            <Json_Card
              id={!empty(item.id) ? item.id : 'No Id'}
              title={!empty(item.title) ? item.title : 'No Name'}
              completed={!empty(item.completed) ? item.completed : 'No Team'}
              profImage={item.profImage}
            />
          );
        }}
      />
    );
  }

  function OnchangeSearch(text) {
    setsearch(text);
  }

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <StatusBar backgroundColor="grey" />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            borderWidth: 1,
            alignItems: 'center',
            flexDirection: 'row',
            padding: 5,
            borderColor: 'black',
            borderRadius: 12,
            height: 50,
            marginEnd: 10,
            marginStart: 10,
            margin: 8,
          }}>
          <Feather
            name="search"
            color="grey"
            size={18}
            style={{marginEnd: 5, marginStart: 5}}
          />
          <TextInput
            style={{
              width: '80%',
              marginRight: 5,
              fontSize: 15,
              color: 'black',
            }}
            placeholder="Search Employee"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={search}
            onChangeText={text => OnchangeSearch(text)}
          />
        </View>
      </View>

      {store.showLoader && <ActivityIndicator size={'large'} />}
      {!store.showLoader && <View style={{flex: 0.9}}>{renderData()}</View>}
    </View>
  );
}
function connectToStore(store) {
  return {
    showLoader: store.empAssReducer.showLoader,
    jsonList: store.empAssReducer.jsonList,
  };
}
