import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import * as counterAction from '../redux/action/CounterAction';
import {getCountAsync, setCountAsync} from '../utils/Storage';

export default function CounterApp() {
  let store = useSelector(connectToStore, shallowEqual);
  let dispatch = useDispatch();
  const [counting, setCounting] = useState(0);
  const [disable, setDisable] = useState(false);

  useEffect(() => {
    async function get() {
      console.log('useState', counting);
      let cc = await getCountAsync('asyncCount');
      console.log('prev cc', cc);
      if (cc != null) {
        dispatch(counterAction.CountVal(cc));
      }
    }
    get();
  }, []);

  useEffect(
    () => {
      function set() {
        setCounting(store.count);
        setCountAsync('asyncCount', store.count);
      }
      set();
    },
    [store.count],
    [counting],
  );

  function onPressIncrease() {
    // if (store.count > 0) {
    //   setDisable(false);
    // }
    dispatch(counterAction.Increment(counting));
  }
  function onPressDecrease() {
    if (store.count < 1) {
      setDisable(true);
    }
    dispatch(counterAction.Decrement(counting));
  }

  return (
    <View style={styles.container}>
      <View
        style={{
          backgroundColor: 'grey',
          flex: 0.1,
        }}>
        <Text
          style={{
            color: 'white',
            alignSelf: 'center',
            fontSize: 50,
          }}>
          Counter App
        </Text>
      </View>
      <View
        style={{
          flex: 0.9,
          backgroundColor: 'grey',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableHighlight onPress={onPressIncrease}>
          <View style={styles.button}>
            <Text style={{color: 'black'}}>INCREMENT</Text>
          </View>
        </TouchableHighlight>

        <View style={{margin: 50}}>
          <Text style={{color: 'white', fontSize: 80}}>{counting}</Text>
        </View>

        {counting > 0 ?<TouchableHighlight onPress={onPressDecrease} disabled={disable}>
          <View style={styles.button}>
            <Text style={{color: 'black'}}>DECREMENT</Text>
          </View>
        </TouchableHighlight>:<TouchableHighlight onPress={onPressDecrease} disabled={disable}>
          <View style={styles.button}>
            <Text style={{color: 'white'}}>DECREMENT</Text>
          </View>
        </TouchableHighlight>}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#e75480',
    padding: 10,
    width: 200,
  },
});

function connectToStore(store) {
  return {
    count: store.counterReducer.count,
  };
}
