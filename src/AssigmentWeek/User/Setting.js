import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import moment from 'moment';
import Feather from 'react-native-vector-icons/dist/Feather';
import {empty} from '../../utils/Validate';
import {getEmpUser, setEmpUser} from '../../utils/Storage';

export default function Setting() {
  const [oldData, setOldData] = useState([]);
  const [newPass, setNewPass] = useState('');
  const [oldPass, setOldPass] = useState('');
  const [conPass, setConPass] = useState('');

  const [showPassword, setShowPassword] = useState(true);

  const [newPassError, setNewPassError] = useState('');
  const [conPassError, setConPassError] = useState('');
  useEffect(() => {
    async function checkOldPass() {
      let loginData = await getEmpUser('key');
      setOldData(loginData);
      console.log('oldData', oldData);
      setOldPass(loginData.password);
      console.log('oldPss', oldPass);
    }
    checkOldPass();
  }, []);

  function onPressChangePass() {
    let isValid = valid();
    if (isValid) {
      oldData.password = conPass;
      console.log('loginData', oldData);
      setEmpUser('key', oldData);
    }
    setNewPass('');
    setConPass('');
  }
  function OnChangeNewPass(text) {
    setNewPass(text);
  }
  function OnChangeConPass(text) {
    setConPass(text);
  }

  function valid() {
    let valid = true;
    let newPassError = '';
    let conPassError = '';
    if (empty(newPass)) {
      valid = false;
      newPassError = 'Enter New Password';
      setNewPassError(newPassError);
    }
    if (empty(conPass)) {
      valid = false;
      conPassError = 'Enter Confirm Password';
      setConPassError(conPassError);
    }
    if (newPass !== conPass) {
      valid = false;
      conPassError = 'Confirm Password not Same';
      setConPassError(conPassError);
    }
    if (oldPass === newPass) {
      valid = false;
      conPassError = 'Same as Old Password';
      setConPassError(conPassError);
    }

    setConPassError('');
    setNewPassError('');

    return valid;
  }

  return (
    <View style={styles.main}>
      <Text style={styles.text_main}>New Password</Text>
      <View style={styles.input}>
        <Feather name="lock" color="white" size={20} />
        <TextInput
          style={styles.textInput}
          placeholder="Enter Password"
          placeholderTextColor="#00000044"
          autoCapitalize="none"
          value={newPass}
          onChangeText={OnChangeNewPass}
        />
      </View>

      {!empty(newPassError) && (
        <View>
          <Text style={styles.errorMsg}>{newPassError}</Text>
        </View>
      )}

      <Text style={styles.text_main}>Confirm Password</Text>
      <View style={styles.input}>
        <Feather name="lock" color="white" size={20} />
        <TextInput
          style={styles.textInput}
          secureTextEntry={showPassword}
          placeholder="Enter Confirm Password"
          placeholderTextColor="#00000044"
          autoCapitalize="none"
          value={conPass}
          onChangeText={OnChangeConPass}
        />
        <TouchableOpacity onPress={() => setShowPassword(!showPassword)}>
          {!showPassword && (
            <Feather name="eye-off" color="#00000044" size={20} />
          )}

          {showPassword && <Feather name="eye" color="#00000044" size={20} />}
        </TouchableOpacity>
      </View>

      {!empty(conPassError) && (
        <View>
          <Text style={styles.errorMsg}>{conPassError}</Text>
        </View>
      )}
      <TouchableOpacity
        style={{
          width: '60%',
          height: 50,
          marginTop: 80,
          justifyContent: 'center',
          alignSelf: 'center',
          borderRadius: 10,
          backgroundColor: '#fff',
        }}
        onPress={onPressChangePass}>
        <Text
          style={{
            fontSize: 18,
            fontWeight: 'bold',
            alignSelf: 'center',
            color: '#629d88',
          }}>
          Change Password
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 3,
    paddingHorizontal: 20,
    paddingVertical: 30,
    backgroundColor: '#629d88',
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_main: {
    fontSize: 18,
    color: 'white',
    marginTop: 30,
  },
  input: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    paddingBottom: 5,
  },

  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 10,
    color: 'white',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 15,
  },
});
