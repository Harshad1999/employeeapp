import {View, StatusBar} from 'react-native';
import React, {useState, useEffect} from 'react';
import {FlatList} from 'react-native-gesture-handler';
import {empty, isArrEmpty} from '../../utils/Validate';
import {useNavigation} from '@react-navigation/native';
import _ from 'lodash';
import Date_Card from '../Components/Date_Card';
import {getPresence, remPresence, setPresence} from '../../utils/Storage';
import moment from 'moment';

export default function Timeline() {
  let navigation = useNavigation();

  const [empData, setEmpData] = useState([]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      async function gettingData() {
        let attendanceData = await getPresence('Key');
        if (!isArrEmpty(attendanceData)) {
          setEmpData(attendanceData);
          console.log('getpress', attendanceData);
        }
      }
      ('Key');
      gettingData();
    });

    return () => {
      unsubscribe;
    };
  }, [navigation]);

  function removeObj(item, index) {
    let temp = [...empData];
    temp.splice(index, 1);
    setEmpData(temp);
    setPresence('Key', temp);
  }
  const numColumns = 3;
  const formatData = (data, numColumns) => {
    const number0fFullRows = Math.floor(data.length / numColumns);
    let number0fElementsLastRow = data.length - number0fFullRows * numColumns;
    while (
      number0fElementsLastRow !== numColumns &&
      number0fElementsLastRow !== 0
    ) {
      data.push({key: `blank-${number0fElementsLastRow}`, empty: true});
      number0fElementsLastRow = number0fElementsLastRow + 1;
    }
    return data;
  };

  function renderData() {
    let newData = _.cloneDeep(empData);

    return (
      <FlatList
        data={formatData(newData, numColumns)}
        numColumns={numColumns}
        keyExtractor={item => item.id}
        renderItem={({item, index}) => {
          if (item.empty === true) {
            return <View style={{flex: 0.42, backgroundColor: 'trasparent'}} />;
          }
          return (
            <Date_Card
              day={
                !empty(moment(item.date).format('DD'))
                  ? moment(item.date).format('DD')
                  : 'No day'
              }
              month={
                !empty(moment(item.date).format('MMMM'))
                  ? moment(item.date).format('MMMM')
                  : 'No Month'
              }
              presence={!empty(item.presence) ? item.presence : 'No presence'}
              OnPressTrash={() => removeObj(item, index)}
            />
          );
        }}
      />
    );
  }

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <StatusBar backgroundColor="grey" />

      {renderData()}
    </View>
  );
}
