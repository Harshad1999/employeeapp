import {View, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import {getEmpUser} from '../../utils/Storage';

export default function WelcomeMain() {
  const [user, setUser] = useState('');
  useEffect(() => {
    async function checkUser() {
      let role = await getEmpUser('key');
      console.log('getuser ka data', role.username);
      setUser(role.username);
    }
    checkUser();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#eee'}}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Text
          style={{
            fontSize: 30,
            alignSelf: 'center',
          }}>
          Welcome
        </Text>
        <Text
          style={{
            fontSize: 30,
            alignSelf: 'center',
          }}>
          {user}
        </Text>
      </View>
    </View>
  );
}
