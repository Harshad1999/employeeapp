import {Text, TouchableOpacity, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import {Decrement} from '../../week2/intro/EcomApp/src/redux/action/CounterAction';

export default class CounterClass extends Component {
  constructor(props) {
    super(props);
    this.state = {count: 0, increaseVal: 0, name: 'Harshad'};
    this.increment = this.increment.bind(this);
  }

  componentDidMount() {
    this.setState((state, props) => ({count: 5}));
  }

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {
    if (prevState.count === 8) {
      this.setState({count: 12});
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.count === 8) {
      console.log('this is 8', nextState);
      return false;
    }
    return true;
  }

  increment() {
    this.setState((state, props) => ({count: this.state.count + 1}));
  }

  decrement() {
    this.setState((state, props) => ({count: this.state.count - 1}));
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={this.increment}>
            <View style={styles.button}>
              <Text style={{color: 'black'}}>INCREMENT</Text>
            </View>
          </TouchableOpacity>

          <View style={{margin: 50}}>
            <Text style={{color: 'grey', fontSize: 80}}>
              {this.state.count}
            </Text>
          </View>

          <TouchableOpacity onPress={this.decrement.bind(this)}>
            <View style={styles.button}>
              <Text style={{color: 'black'}}>DECREMENT</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'red',
    borderRadius: 25,
    padding: 10,
    width: 150,
  },
});
