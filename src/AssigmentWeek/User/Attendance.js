import {View, Text, ToastAndroid, TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';
import {Picker} from '@react-native-picker/picker';
import Date_Card from '../Components/Date_Card';
import _ from 'lodash';
import Feather from 'react-native-vector-icons/dist/Feather';

// import DatePicker from 'react-native-date-picker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

import moment from 'moment';
import {getPresence, setPresence} from '../../utils/Storage';
import {empty} from '../../utils/Validate';

export default function Attendance() {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [date, setDate] = useState('');
  const [selectedValue, setSelectedValue] = useState('Present');
  const handleConfirm = date => {
    console.log('A date has been picked: ', moment(date).format('MMMM'));
    setDate(date);
    hideDatePicker();
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const showToast = () => {
    if (selectedValue === 'Leave') {
      ToastAndroid.show('Leave Applied', ToastAndroid.SHORT);
    }
    ToastAndroid.show('Marked Present', ToastAndroid.SHORT);
  };

  async function OnPressApply() {
    let attendanceData = {
      id: Math.random(),
      date: date,
      presence: selectedValue,
    };

    let oldData = await getPresence('Key');

    if (empty(oldData)) {
      oldData = [];
    }
    oldData.push(attendanceData);

    setPresence('Key', oldData);
    showToast();
  }

  return (
    <View style={{flex: 1, justifyContent: 'center', alignContent: 'center'}}>
      <View
        style={{
          flex: 0.5,
          backgroundColor: '#629d88',
          margin: 25,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 30,
        }}>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{
              fontSize: 19,
              color: '#fff',
              marginRight: 12,
            }}>
            Select Date :
          </Text>
          <View
            style={{
              flexDirection: 'row',
              width: '50%',
              height: 40,
              justifyContent: 'space-evenly',
              alignItems: 'center',
              borderRadius: 10,
              backgroundColor: '#fff',
            }}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: '#629d88',
              }}>
              {moment(date).format('DD/MM/YYYY')
                ? moment(date).format('DD/MM/YYYY')
                : 'Select Date'}
            </Text>
            <TouchableOpacity>
              <Feather
                name="calendar"
                color="black"
                size={20}
                onPress={showDatePicker}
              />
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
          </View>
        </View>
        {/* <Text
          style={{
            fontSize: 19,
            color: '#fff',
            marginRight: 12,
          }}>
          {moment(date).format('DD/MM/YYYY')}
        </Text> */}
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <Text
            style={{
              fontSize: 19,
              marginRight: 12,
              marginTop: 10,
              color: '#fff',
            }}>
            Punch In      :
          </Text>
          <Picker
            style={{
              backgroundColor: 'white',
              // borderRadius: 25,
              color: 'black',
              fontSize: 19,
              width: '50%',
              alignSelf: 'center',
            }}
            selectedValue={selectedValue}
            onValueChange={(value, index) => setSelectedValue(value)}>
            <Picker.Item label="Present" value="Present" />
            <Picker.Item label="Leave" value="Leave" />
          </Picker>
        </View>
        <TouchableOpacity
          style={{
            width: '40%',
            height: 50,
            marginTop: 90,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            backgroundColor: '#fff',
          }}
          onPress={OnPressApply}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              color: '#629d88',
            }}>
            Apply {/* Apply text */}
          </Text>
          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 0.5,
          backgroundColor: '#629d88',
          marginTop: 15,
          marginLeft: 15,
          marginRight: 15,
          padding: 20,
          justifyContent: 'center',
          alignItems: 'center',
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}>
        {/* <Text
          style={{
            fontSize: 19,
            color: '#fff',
            marginRight: 12,
          }}>
          {moment(date).format('DD/MM/YYYY')}
        </Text> */}
      </View>
    </View>
  );
}
