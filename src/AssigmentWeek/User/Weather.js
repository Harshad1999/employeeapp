import {View, Text, ImageBackground, StyleSheet} from 'react-native';
import React, {useState, useEffect} from 'react';
import Header_Card from '../Components/Header_Card';
import Footer_Card from '../Components/Footer_Card';
import Geolocation from 'react-native-geolocation-service';

import axios from 'axios';

import _ from 'lodash';

const API_key = '768d47b60f8d4c6e8ea7d81e6359f4e7';

const image = {
  uri: 'https://www.teahub.io/photos/full/94-947156_1080-1920-hd-nature.jpg',

  // uri: 'https://www.fonewalls.com/wp-content/uploads/2019/09/Sea-wave-Wallpaper-300x585.jpg',
};
export default function Weather() {
  const [data, setData] = useState({});

  useEffect(() => {
    Geolocation.getCurrentPosition(
      success => {
        console.log('successsssssssssss', success.coords);
        let {latitude, longitude} = success.coords;
        getWeather(latitude, longitude);
      },
      err => {
        getWeather('19.0760', '72.8777');
      },
    );
  }, []);

  function getWeather(lat, lon) {
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=hourly,minutely&units=metric&appid=${API_key}`,
      )
      .then(response => {
        setData(response.data);
      });
  }

  return (
    <View style={styles.container}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#18181b66',
          }}>
          <Header_Card
            current={data.current}
            timezone={data.timezone}
            lat={data.lat}
            lon={data.lon}
          />
          <Footer_Card weatherData={data.daily} />
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    fontSize: 42,
    lineHeight: 84,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: '#000000c0',
  },
});
