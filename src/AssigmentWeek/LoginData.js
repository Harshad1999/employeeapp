export function getLoginData() {
  const loginData = [
    {
      username: 'test',
      password: 'test',
      access: 'admin',
    },
    {
      username: 'testuser',
      password: 'testuser',
      access: 'user',
    },
    {
      username: 'Harshad@v2stech.com',
      password: '123456',
      access: 'admin',
    },
    {
      username: 'Shubham@v2stech.com',
      password: '1234',
      access: 'user',
    },
    {
      username: 'Vinit@v2stech.com',
      password: '123',
      access: 'user',
    },
  ];
  return loginData;
}
