import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import React, {useState, useEffect} from 'react';
import Feather from 'react-native-vector-icons/Feather';

export default function Card(props) {
  return (
    <View style={style.container}>
      <View style={style.imgview}>
        <Image style={style.image} source={{uri: props.imageuri}} />
      </View>
      <View style={style.prodetview}>
        <Text style={style.proddname}>{props.prodname}</Text>
        <Text style={style.proddescgrey}>{props.proddesc}</Text>

        <View style={style.bottomview}>
          <Text style={style.price}>₹ {props.price}</Text>

          <View style={style.button}>
            <Feather
              onPress={props.onPressPlus}
              name="plus-circle"
              style={{fontSize: 25, color: 'white'}}
            />
            {/* <Text style={{color: 'white'}}>Buy</Text> */}
          </View>
          <View>
            <Text
              style={{
                color: 'white',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 15,
              }}>
              {props.qty}
            </Text>
          </View>

          <View style={style.button}>
            <Feather
              onPress={props.onPressMinus}
              name="minus-circle"
              style={{fontSize: 25, color: 'white'}}
            />
          </View>
          <View style={style.button}>
            <Feather
              onPress={props.onPressTrash}
              name="trash"
              style={{fontSize: 25, color: 'white'}}
            />
            {/* <Text style={{color: 'white'}}>Buy</Text> */}
          </View>
        </View>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    margin: 15,
    padding: 10,
    height: 130,

    backgroundColor: '#303030',
    borderRadius: 15,
  },
  button: {
    // backgroundColor: '#6466f1',
    alignItems: 'center',
    justifyContent: 'center',
    // margin:,
    borderRadius: 10,
    // height: 40,
    // width: 70,
  },
  bottomview: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  proddname: {
    fontSize: 25,
    color: 'white',
  },
  image: {
    width: 90,
    resizeMode: 'contain',
  },
  proddescgrey: {
    fontSize: 17,
    color: '#828282',
  },
  price: {
    marginTop: 10,
    color: 'green',
    fontSize: 19,
  },
  imgview: {
    flex: 0.4,
    flexDirection: 'row',
  },
  prodetview: {
    flex: 0.7,
  },
});
