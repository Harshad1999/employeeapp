import {
  StyleSheet,
  Modal,
  TouchableOpacity,
  SafeAreaView,
  View,
  Text,
} from 'react-native';
import React, {useState} from 'react';

export default function dropDown() {
  const [chooseRole, setChooseRole] = useState('Select Role...');
  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity style={styles.touchableOpacity}>
        <Text style={styles.text}>{chooseRole}</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {},
  touchableOpacity: {},
  text: {},
});
