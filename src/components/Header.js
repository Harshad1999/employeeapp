import {View, Text} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';

export default function Header(props) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        // alignItems: 'center',
        // alignContent: 'space-around',
      }}>
      <Feather name="menu" color="black" size={20} style={{marginLeft: 10}} />
      <Text
        style={{
          color: 'black',
          fontSize: 20,
          fontWeight: '400',
        }}>
        Header
      </Text>
      <Feather name="user" color="black" size={20} style={{marginRight: 10}} />
    </View>
  );
}
