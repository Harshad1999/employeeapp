import {applyMiddleware, createStore} from 'redux';
import reducer from './reducer/index';
import thunkMiddleWare from 'redux-thunk';
import promise from 'redux-promise-middleware';


export default function ConfigStore() {
  const store = createStore(reducer, applyMiddleware(promise,thunkMiddleWare));

  return store;
}

