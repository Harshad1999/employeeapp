import {combineReducers} from 'redux';

import cartReducer from './CartReducer';
import counterReducer from './CounterReducer';
import empCardReducer from './EmpCardReducer';
import empAssReducer from './EmpAssReducer';

const reducerState = combineReducers({
  cartReducer,
  counterReducer,
  empCardReducer,
  empAssReducer,
});

const newState = (state, action) => {
  return reducerState(state, action);
};

export default newState;
