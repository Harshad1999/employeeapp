import * as actions from '../../utils/Action';
import _ from 'lodash';

const initialState = {
  EmpList: [],
};

export default function CounterReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_DATA:
      let tempArr = _.cloneDeep(state.EmpList);
      console.log('temparr', tempArr);
      tempArr.push(action.payload);
      return {
        ...state,
        EmpList: tempArr,
      };

    default:
      return {...state};
  }
}
