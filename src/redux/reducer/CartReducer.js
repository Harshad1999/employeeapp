import * as actions from '../../utils/Action';
import _ from 'lodash';

const initialState = {
  deleteMsg: '',
  addToCart: '',
  name: '',
  count: 0,
  userList: [],
  loginList: [],
  isDeleted: null,
  showLoader: false,
};

export default function CartReducer(state = initialState, action) {
  switch (action.type) {
    case 'DELETE_ITEM':
      return {
        ...state,
        deleteMsg: action.payload,
      };

    case 'ADD_TO_CART':
      return {
        ...state,
        addToCart: 'Added Successfully',
      };

    case 'DELETE_ITEM_FAILED':
      let parseData = JSON.parse(action.payload);
      return {
        ...state,
        deleteMsg: parseData.message,
      };

    case 'GET_USER_LIST':
      return {
        ...state,
        deleteMsg: JSON.stringify(action.payload),
      };

    case actions.pending(actions.GET_USER_USER_LIST):
      return {
        ...state,
        userList: [],
        showLoader: true,
      };

    case actions.fulfilled(actions.GET_USER_USER_LIST):
      return {
        ...state,
        userList: action.payload.data,
        showLoader: false,
      };

    case actions.rejected(actions.GET_USER_USER_LIST):
      console.log('REJECTED ERROR');
      return {
        ...state,
        showLoader: false,
      };

    case actions.pending(actions.LOGIN_DATA):
      return {
        ...state,
        loginList: [],
        showLoader: true,
      };

    case actions.fulfilled(actions.LOGIN_DATA):
      return {
        ...state,
        loginList: action.payload.data,
        showLoader: false,
      };

    case actions.rejected(actions.LOGIN_DATA):
      return {
        ...state,
        showLoader: false,
      };
    case actions.pending(actions.LOGIN_DATA_POST):
      return {
        ...state,
        showLoader: true,
      };

    case actions.fulfilled(actions.LOGIN_DATA_POST):
      return {
        ...state,
        showLoader: false,
      };

    case actions.rejected(actions.LOGIN_DATA_POST):
      return {
        ...state,
        showLoader: false,
      };
    case actions.pending(actions.LOGIN_DATA_DELETE):
      return {
        ...state,
        showLoader: true,
        // isDeleted: null,
      };

    case actions.fulfilled(actions.LOGIN_DATA_DELETE):
      let object = action.payload.data;
      let tempList = _.cloneDeep(state.loginList);
      if (object !== null) {
        let index = _.findIndex(tempList, i => {
          return i.id === object.id;
        });

        if (index > -1) {
          tempList.splice(index, 1);
          // tempList[index] = object;
        }
      }

      console.log('ddddsdsd', action.payload.data);
      return {
        ...state,
        loginList: tempList,
        showLoader: false,
      };

    case actions.rejected(actions.LOGIN_DATA_DELETE):
      console.log('REJECTED ERROR');
      return {
        ...state,
        showLoader: false,
      };

    case actions.pending(actions.LOGIN_DATA_PUT):
      return {
        ...state,
        showLoader: true,
        // isDeleted: null,
      };

    case actions.fulfilled(actions.LOGIN_DATA_PUT):
      let updateobject = action.payload.data;
      let updatetempList = _.cloneDeep(state.loginList);
      if (updateobject !== null) {
        let index = _.findIndex(updatetempList, i => {
          return i.id === updateobject.id;
        });

        if (index > -1) {
          // tempList.splice(index, 1);
          updatetempList[index] = object;
        }
      }

      console.log('put', action.payload.data);
      return {
        ...state,
        loginList: tempList,
        showLoader: false,
      };

    case actions.rejected(actions.LOGIN_DATA_PUT):
      console.log('REJECTED ERROR');
      return {
        ...state,
        showLoader: false,
      };

    default:
      return {...state};
  }
}
