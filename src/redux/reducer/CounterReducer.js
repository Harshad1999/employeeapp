import * as actions from '../../utils/Action';

const initialState = {
  count: 0,
 
};

export default function CounterReducer(state = initialState, action) {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        count: action.payload + 1,
      };

    case actions.DECREMENT:
      if (state.count > 0) {
        return {
          ...state,
          count: action.payload - 1,
        };
      }

    case actions.COUNTVAL:
      return {
        ...state,
        count: action.payload,
      };

    default:
      return {...state};
  }
}
