import * as actions from '../../utils/Action';
import _ from 'lodash';

const initialState = {
  jsonList: [],
  weatherList: [],
  showLoader: false,
};

export default function CartReducer(state = initialState, action) {
  switch (action.type) {
    case actions.pending(actions.GET_JSON):
      return {
        ...state,
        jsonList: [],
        showLoader: true,
      };

    case actions.fulfilled(actions.GET_JSON):
      return {
        ...state,
        jsonList: action.payload.data,
        showLoader: false,
      };

    case actions.rejected(actions.GET_JSON):
      console.log('REJECTED ERROR');
      return {
        ...state,
        showLoader: false,
      };
    case actions.pending(actions.GET_WEATHER):
      return {
        ...state,
        weatherList: [],
        showLoader: true,
      };

    case actions.fulfilled(actions.GET_WEATHER):
      // console.log('weatherrrr data', action.payload.data);
      return {
        ...state,
        weatherList: action.payload.data,
        showLoader: false,
      };

    case actions.rejected(actions.GET_WEATHER):
      console.log('REJECTED ERROR');
      return {
        ...state,
        showLoader: false,
      };

    default:
      return {...state};
  }
}
