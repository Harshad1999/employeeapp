import {request} from '../../Services/Request';
import {axiosurl, weatherApi} from '../../utils/Url';

import * as actions from '../../utils/Action';
const API_key = '768d47b60f8d4c6e8ea7d81e6359f4e7';
export function getJson() {
  return async dispatch => {
    let httpReq = await request(axiosurl);
    dispatch({
      type: actions.GET_JSON,
      payload: httpReq.get('/todos'),
    });
  };
}

export function getWeather(lat, lon) {
  return async dispatch => {
    let httpReq = await request(weatherApi);
    dispatch({
      type: actions.GET_WEATHER,
      payload: httpReq.get(
        `/onecall?lat=${lat}&lon=${lon}&exclude=hourly,minutely,current,alert&units=metric&appid=${API_key}`,
      ),
    });
  };
}
