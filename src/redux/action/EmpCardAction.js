import * as actions from '../../utils/Action';

export function setData(data) {
  return dispatch => {
    dispatch({
      type: actions.SET_DATA,
      payload: data,
    });
  };
}
