import * as actions from '../../utils/Action';

export function Increment(count) {
  return dispatch => {
    dispatch({
      type: 'INCREMENT',
      payload: count,
    });
  };
}
export function Decrement(count) {
  return dispatch => {
    dispatch({
      type: actions.DECREMENT,
      payload: count,
    });
  };
}

export function CountVal(count) {
  return dispatch => {
    dispatch({
      type: actions.COUNTVAL,
      payload: count,
    });
  };
}
