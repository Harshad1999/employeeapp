import {request} from '../../Services/Request';
import {axiosurl, loginUrl, MyUrl, sankalpBaseUrl} from '../../utils/Url';

import * as actions from '../../utils/Action';

export function deleteEmploye(data) {
  return dispatch => {
    dispatch({
      type: 'DELETE_ITEM',
      payload: data,
    });
  };
}

export function getUser(id) {
  return async dispatch => {
    let http = await request(sankalpBaseUrl);
    dispatch({
      type: 'GET_USER_LIST',
      payload: http.get(`/checkout/sdk/demo?user_id=${id}`),
    });
  };
}

export function getUserUser() {
  return async dispatch => {
    let httpReq = await request(MyUrl);
    dispatch({
      type: actions.GET_USER_USER_LIST,
      payload: httpReq.get('/users'),
    });
  };
}


export function getUserLogin() {
  return async dispatch => {
    let httpReq = await request(loginUrl);
    dispatch({
      type: actions.LOGIN_DATA,
      payload: httpReq.get('/login'),
    });
  };
}

export function setUserLogin(newEmpData) {
  return async dispatch => {
    let httpReq = await request(loginUrl);
    dispatch({
      type: actions.LOGIN_DATA_POST,
      payload: httpReq.post('/login', newEmpData),
    });
  };
}

export function delUserLogin(id) {
  return async dispatch => {
    let httpReq = await request(loginUrl);
    dispatch({
      type: actions.LOGIN_DATA_DELETE,
      payload: httpReq.delete(`/login/${id}`),
    });
  };
}


export function putUserLogin(id) {
  return async dispatch => {
    let httpReq = await request(loginUrl);
    dispatch({
      type: actions.LOGIN_DATA_PUT,
      payload: httpReq.put(`/login/${id}`),
    });
  };
}
