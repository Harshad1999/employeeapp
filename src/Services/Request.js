import axios from 'axios';

export function request(baseUrl) {
  let request = axios.create({
    baseURL: baseUrl,
    timeout: 90000,
  });

  request.interceptors.response.use(
    function (response) {
      var data = response.data;

      return {data: data};
    },
    function (error) {
      console.log(JSON.stringify(error));
      message = error.message;

      return Promise.reject({status: 'error', message: message});
    },
  );

  return request;
}
