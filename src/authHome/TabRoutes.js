import React, {useEffect, useState} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AddEmp from '../Pages/AddEmp';
import SearchEmp from '../Pages/SearchEmp';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import Header from '../components/Header';
import MainHome from '../Pages/MainHome';
import {getUser} from '../utils/Storage';
import {empty} from '../utils/Validate';
import EmpFormClass from '../ClassComponent/EmpFormClass';
import EmpListClass from '../ClassComponent/EmpListClass';
import EmpDetailClass from '../ClassComponent/EmpDetailClass';

const Tab = createBottomTabNavigator();

export default function TabRoutes() {
  const [userRole, setUerRole] = useState('');
  useEffect(() => {
    async function checkUserRole() {
      let role = await getUser();
      setUerRole(role.access);
    }
    checkUserRole();
  }, []);
  return (
    <Tab.Navigator
      initialRouteName="EmployeesList"
      screenOptions={{
        header: props => <Header {...props} />,
        tabBarActiveTintColor: '#629d88',
      }}>
      {/* {!empty(userRole) && userRole === 'admin' && (
      <Tab.Screen
        name="AddEmployee"
        component={AddEmp}
        options={{
          tabBarLabel: 'Add',
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="playlist-add" color={color} size={size} />
          ),
        }}
      />
      )} */}
      {/* <Tab.Screen
        name="Employees"
        component={MainHome}
        options={{
          tabBarLabel: 'List',
          tabBarIcon: ({color, size}) => (
            <Feather name="list" color={color} size={size} />
          ),
        }}
      /> */}
      <Tab.Screen
        name="EmployeesList"
        component={EmpListClass}
        options={{
          tabBarLabel: 'List',
          tabBarIcon: ({color, size}) => (
            <Feather name="list" color={color} size={size} />
          ),
        }}
      />
      
      <Tab.Screen
        name="EmployeesForm"
        component={EmpFormClass}
        options={{
          tabBarLabel: 'Add',
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="playlist-add" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Search Employee"
        component={SearchEmp}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="search" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
