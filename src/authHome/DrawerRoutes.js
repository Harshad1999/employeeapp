import React from 'react';
import Profile from '../Pages/Profile';
import AboutUS from '../Pages/AboutUS';
import ContactUs from '../Pages/ContactUs';
import Main from '../Pages/Main';
import Cart from '../Pages/Catalog';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Logout from '../Pages/Logout';
import MainHome from '../Pages/MainHome';
import EmpProfile from '../Pages/EmpProfile';
import TabRoutes from './TabRoutes';
import CounterApp from '../AssigmentWeek/CounterApp';

const Drawer = createDrawerNavigator();

export default function DrawerRoutes() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen
        name="Home"
        component={TabRoutes}
        options={{headerShown: false}}
      />
      <Drawer.Screen
        name="Counter"
        component={CounterApp}
        options={{headerShown: false}}
      />
      <Drawer.Screen
        name="Main"
        component={Main}
        options={{headerShown: false}}
      />
      <Drawer.Screen
        name="Cart"
        component={Cart}
        options={{headerShown: false}}
      />
      <Drawer.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />
      <Drawer.Screen
        name="AboutUs"
        component={AboutUS}
        options={{headerShown: false}}
      />
      <Drawer.Screen
        name="ContactUs"
        component={ContactUs}
        options={{headerShown: false}}
      />
      <Drawer.Screen
        name="Logout"
        component={Logout}
        options={{headerShown: false}}
      />
    </Drawer.Navigator>
  );
}
