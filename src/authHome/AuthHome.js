import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import LoginScreen from '../Pages/LoginScreen';

import TabRoutes from './TabRoutes';
import EmpProfile from '../Pages/EmpProfile';

import MainHome from '../Pages/MainHome';
import DrawerRoutes from './DrawerRoutes';
import AddEmp from '../Pages/AddEmp';
import LoginClass from '../Pages/LoginClass';
import EmpCardClass from '../ClassComponent/EmpCardClass';
import EmpFormClass from '../ClassComponent/EmpFormClass';
import EmpListClass from '../ClassComponent/EmpListClass';
import EmpDetailClass from '../ClassComponent/EmpDetailClass';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function AuthHome() {
  return (
    <Stack.Navigator initialRouteName="LoginClass">
      {/* <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{headerShown: false}}
      /> */}
      {/* <Stack.Screen
        name="LoginClass"
        component={LoginClass}
        options={{headerShown: false}}
      /> */}

      <Stack.Screen
        name="Drawer"
        component={DrawerRoutes}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EmpDetailClass"
        component={EmpDetailClass}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EmpProfile"
        component={EmpProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AddEmployee"
        component={AddEmp}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Home" component={MainHome} />
    </Stack.Navigator>
  );
}
