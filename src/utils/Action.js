export const GET_USER_USER_LIST = 'GET_USER_USER_LIST';
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

export const SET_DATA = 'SET_DATA';

export const LOGIN_DATA = 'LOGIN_DATA';

export const LOGIN_DATA_POST = 'LOGIN_DATA_POST';

export const LOGIN_DATA_PUT = 'LOGIN_DATA_PUT';

export const COUNTVAL = 'COUNTVAL';

export const GET_JSON = 'GET_JSON';

export const GET_WEATHER = 'GET_WEATHER';

export const LOGIN_DATA_DELETE = 'LOGIN_DATA_DELETE';

export function pending(action) {
  return action + '_PENDING';
}

export function fulfilled(action) {
  return action + '_FULFILLED';
}
export function rejected(action) {
  return action + '_REJECTED';
}
