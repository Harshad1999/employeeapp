import {empty} from './Validate';

export function inputValidation(isRequired, message) {
  let messageValue = 'Field is required';
  if (!empty(message)) messageValue = message;
  return {
    required: isRequired,
    message: messageValue,
  };
}

export function lengthValidation(isRequired, message) {
  let messageValue = 'Field is required';
  if (!empty(message)) messageValue = message;
  return {
    required: isRequired,
    message: messageValue,
    type: 'length',
  };
}
