export function getCardData() {
  const cardarr = [
    {
      key: Math.random(),
      prodname: 'Iphone 13 Pro',

      price: 900,
      qty: 1,
    },
    {
      key: Math.random(),
      prodname: 'Iphone 13',
      proddesc: '6gb/128GB',
      price: 200,
      qty: 1,
    },
    {
      key: Math.random(),
      prodname: 'Iphone 12',
      proddesc: '6gb/128GB',
      price: 10,
      qty: 1,
    },

    // {
    //   key: Math.random(),
    //   prodname: 'Iphone SE',
    //   proddesc: '6gb/128GB',
    //   price: '300.00',
    //   qty: 0,
    // },
    // {
    //   key: Math.random(),
    //   prodname: 'Iphone SE',
    //   proddesc: '6gb/128GB',
    //   price: '300.00',
    //   qty: 0,
    // },
  ];
  return cardarr;
}
