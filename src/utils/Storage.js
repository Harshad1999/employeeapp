import AsyncStorage from '@react-native-async-storage/async-storage';
const userkey = 'MyUserKey';
export async function setItem(key, cardDataAsync) {
  await AsyncStorage.setItem(key, JSON.stringify(cardDataAsync));
}

export async function getItem(key) {
  try {
    let cardDataAsync = await AsyncStorage.getItem(key);
    return JSON.parse(cardDataAsync);
  } catch (e) {
    return null;
  }
}

export async function setUser(userDataAsync) {
  await AsyncStorage.setItem(userkey, JSON.stringify(userDataAsync));
}

export async function getUser() {
  try {
    let userDataAsync = await AsyncStorage.getItem(userkey);
    return JSON.parse(userDataAsync);
  } catch (e) {
    return null;
  }
}
export async function removeUser() {
  try {
    await AsyncStorage.removeItem(userkey);
    return true;
  } catch (exception) {
    return false;
  }
}

export async function setEmp(empKey, empDataAsync) {
  await AsyncStorage.setItem(empKey, JSON.stringify(empDataAsync));
  console.log('Emp data set', empDataAsync);
}

export async function getEmp(empKey) {
  try {
    let empDataAsync = await AsyncStorage.getItem(empKey);
    return JSON.parse(empDataAsync);
  } catch (e) {
    return null;
  }
}
export async function removeEmp(remEmpKey) {
  try {
    await AsyncStorage.removeItem(remEmpKey);
    return true;
  } catch (exception) {
    return false;
  }
}

export async function setCountAsync(countkey, counterAsync) {
  await AsyncStorage.setItem(countkey, JSON.stringify(counterAsync));
  console.log('counterset', counterAsync);
}

export async function getCountAsync(countkey) {
  try {
    let counterAsync = await AsyncStorage.getItem(countkey);
    return JSON.parse(counterAsync);
  } catch (e) {
    return null;
  }
}

export async function setEmpAsync(employeeKey, empAsync) {
  await AsyncStorage.setItem(employeeKey, JSON.stringify(empAsync));
  console.log('Emp data set', empAsync);
}

export async function getEmpAsync(employeeKey) {
  try {
    let empAsync = await AsyncStorage.getItem(employeeKey);
    return JSON.parse(empAsync);
  } catch (e) {
    return null;
  }
}
export async function removeEmpAsync(remEmpKey) {
  try {
    await AsyncStorage.removeItem(remEmpKey);
    return true;
  } catch (exception) {
    return false;
  }
}

export async function setEmpUser(Key, userDataAsync) {
  await AsyncStorage.setItem(Key, JSON.stringify(userDataAsync));
  console.log('Emp data set', userDataAsync);
}

export async function getEmpUser(Key) {
  try {
    let userDataAsync = await AsyncStorage.getItem(Key);
    return JSON.parse(userDataAsync);
  } catch (e) {
    return null;
  }
}
export async function removeEmpUser(Key) {
  try {
    await AsyncStorage.removeItem(Key);
    return true;
  } catch (exception) {
    return false;
  }
}

export async function setPresence(Key, empDataAsync) {
  await AsyncStorage.setItem(Key, JSON.stringify(empDataAsync));
}

export async function getPresence(Key) {
  try {
    let empDataAsync = await AsyncStorage.getItem(Key);
    return JSON.parse(empDataAsync);
  } catch (e) {
    return null;
  }
}

export async function remPresence(Key) {
  try {
    await AsyncStorage.removeItem(Key);
    return true;
  } catch (exception) {
    return false;
  }
}
