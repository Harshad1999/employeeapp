export function empty(value) {
  return value === null || value === undefined || value === '';
}

export function isArrEmpty(value) {
  return value === undefined || value.length === 0;
}

export function valAlpha(value) {
  let alphaReg = /^[a-zA-Z\s]+$/;
  return value.toString().match(alphaReg);
}
export function valAlphaNum(value) {
  let alphaNumReg = /^([a-zA-Z0-9\s _-]+)$/;
  return value.toString().match(alphaNumReg);
}

export function valEmail(value) {
  let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return value.toString().match(emailReg);
}

export function valNum(value) {
  console.log('price ', value);
  // let numreg = /[ a-zA-Z`!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/;
  let numreg = /^[0-9]*\.?[0-9]*$/;
  // if (value.toString().match(numreg)) {
  //   return true;
  // }
  // return false;
  console.log(value.toString().match(numreg));
  return value.toString().match(numreg);

  // n.match(/[ a-zA-Z`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/);
}
