import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ToastAndroid,
} from 'react-native';
import React, {useEffect, useState} from 'react';

import {empty, valAlpha, valAlphaNum, valNum} from '../utils/Validate';
import {getEmp, setEmp} from '../utils/Storage';
import Feather from 'react-native-vector-icons/dist/Feather';
import ImagePicker from 'react-native-image-crop-picker';
import {useNavigation} from '@react-navigation/native';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import * as empCardAction from '../redux/action/EmpCardAction';
import * as cartAction from '../redux/action/CartAction';

export default function AddEmp(props) {
  let store = useSelector(connectToStore, shallowEqual);
  let dispatch = useDispatch();

  const [empUsernameValue, setEmpUsernameValue] = useState({
    value: '',
    error: '',
  });
  const [empPasswordValue, setEmpPasswordValue] = useState({
    value: '',
    error: '',
  });
  const [empNameValue, setEmpNameValue] = useState({
    value: '',
    error: '',
  });
  const [empTeamValue, setEmpTeamValue] = useState({
    value: '',
    error: '',
  });
  const [empRoleValue, setEmpRoleValue] = useState({
    value: '',
    error: '',
  });
  const [empBioValue, setEmpBioValue] = useState({
    value: '',
    error: '',
  });

  const [empData, setEmpData] = useState([]);

  const [image, setImage] = useState('');
  let navigation = useNavigation();

  useEffect(() => {
    return () => {
      clearValue();
    };
  }, [navigation]);

  function clearValue() {
    setEmpUsernameValue('');
    setEmpPasswordValue('');
    setEmpNameValue('');
    setEmpTeamValue('');
    setEmpRoleValue('');
    setImage('');
    setEmpBioValue('');
  }

  const showToastError = () => {
    ToastAndroid.show('Nothing was Added', ToastAndroid.SHORT);
  };
  const showToastSuccess = () => {
    ToastAndroid.show('Data Added Successfully', ToastAndroid.SHORT);
  };
  async function OnPressAddToList() {
    let isValid = formValid();
    if (isValid) {
      let newEmpData = {
        username: empUsernameValue.value,
        password: empPasswordValue.value,
        name: empNameValue.value,
        // team: empTeamValue.value,
        role: empRoleValue.value,
        profImage: image
          ? image
          : 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png',

        // bio: empBioValue.value,
      };

      // let oldData = await getEmp('empKey');
      // let oldData = store.EmpList;
      // console.log(oldData);

      // // let temp = [...empData];
      // if (empty(oldData)) {
      //   oldData = [];
      // }
      // oldData.push(newEmpData);
      // // setEmpData(temp);
      // setEmp('empKey', oldData);
      // clearValue();
      showToastSuccess(cartAction.setUserLogin(newEmpData));
      dispatch(cartAction.setUserLogin(newEmpData));
      console.log(store.loginList);
      // showToastSuccess(cartReducer.setUserLogin(newEmpData));
      // get
      // console.log('onpress add', temp);
    } else showToastError();
  }

  function formValid() {
    let valid = true;

    if (empty(empUsernameValue.value)) {
      valid = false;
    }
    if (empty(empPasswordValue.value)) {
      valid = false;
    }
    if (empty(empNameValue.value)) {
      valid = false;
    }
    // if (empty(empTeamValue.value)) {
    //   valid = false;
    // }
    if (empty(empRoleValue.value)) {
      valid = false;
    }
    // if (empty(empBioValue.value)) {
    //   valid = false;
    // }

    return valid;
  }

  function OnChangeUsernameValue(v) {
    let error = '';
    let value = v;
    if (!valAlphaNum(v)) {
      error = 'Enter valid Username';
      value = '';
    }
    setEmpUsernameValue({...empUsernameValue, value: value, error: error});
  }
  function OnChangePasswordValue(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Password';
      value = '';
    }
    setEmpPasswordValue({...empPasswordValue, value: value, error: error});
  }
  function OnChangeNameText(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Name';
      value = '';
    }
    setEmpNameValue({...empNameValue, value: value, error: error});
  }
  function OnChangeTeamValue(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Team';
      value = '';
    }
    setEmpTeamValue({...empTeamValue, value: value, error: error});
  }
  function OnChangeRoleText(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Role';
      value = '';
    }
    setEmpRoleValue({...empRoleValue, value: value, error: error});
  }
  function OnChangeBioText(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter Bio';
      value = '';
    }
    setEmpBioValue({...empBioValue, value: value, error: error});
  }

  async function OpenCamera() {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      setImage(image.path);
    });
  }

  async function pickImage() {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      setImage(image.path);
    });
  }

  return (
    <View style={styles.container}>
      <View style={{flex: 0.4, alignItems: 'center'}}>
        <Image
          source={{
            uri: image
              ? image
              : 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png',
          }}
          style={styles.profileImg}
        />
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => {
              OpenCamera();
            }}>
            <Feather
              name="camera"
              size={20}
              style={{color: 'black', marginRight: 100}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              pickImage();
            }}>
            <Feather name="image" size={20} style={{color: 'black'}} />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView style={styles.main}>
        <Text style={styles.text_main}>Username : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              placeholder="Harshadxyz"
              placeholderTextColor="#D0D0D0"
              // keyboardType="numb"
              value={empUsernameValue.value}
              onChangeText={OnChangeUsernameValue}
            />
            {!empty(empUsernameValue.error) && (
              <Text style={styles.errorMsg}>{empUsernameValue.error}</Text>
            )}
          </View>
        </View>
        <Text style={styles.text_main}>Password : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              placeholder="eg: John"
              placeholderTextColor="#D0D0D0"
              autoCapitalize="none"
              // keyboardType="number-pad"
              value={empPasswordValue.value}
              onChangeText={OnChangePasswordValue}
            />
            {!empty(empPasswordValue.error) && (
              <Text style={styles.errorMsg}>{empPasswordValue.error}</Text>
            )}
          </View>
        </View>
        <Text style={styles.text_main}>Employee Name : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              placeholder="eg: John"
              placeholderTextColor="#D0D0D0"
              autoCapitalize="none"
              value={empNameValue.value}
              onChangeText={OnChangeNameText}
            />
            {!empty(empNameValue.error) && (
              <Text style={styles.errorMsg}>{empNameValue.error}</Text>
            )}
          </View>
        </View>
        {/* <Text style={styles.text_main}>Team: </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              placeholder="eg: React Native"
              placeholderTextColor="#D0D0D0"
              autoCapitalize="none"
              value={empTeamValue.value}
              onChangeText={OnChangeTeamValue}
            />
            {!empty(empTeamValue.error) && (
              <Text style={styles.errorMsg}>{empTeamValue.error}</Text>
            )}
          </View>
        </View> */}
        <Text style={styles.text_main}>Role : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              style={styles.textInput}
              placeholder="eg: admin"
              placeholderTextColor="#D0D0D0"
              autoCapitalize="none"
              value={empRoleValue.value}
              onChangeText={OnChangeRoleText}
            />
            {!empty(empRoleValue.error) && (
              <Text style={styles.errorMsg}>{empRoleValue.error}</Text>
            )}
          </View>
        </View>
        {/* <Text style={styles.text_main}>Bio : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              multiline
              style={styles.textInput}
              placeholder="eg: Software Developer"
              placeholderTextColor="#D0D0D0"
              autoCapitalize="none"
              value={empBioValue.value}
              onChangeText={OnChangeBioText}
            />
            {!empty(empBioValue.error) && (
              <Text style={styles.errorMsg}>{empBioValue.error}</Text>
            )}
          </View>
        </View> */}
        <View style={styles.button}>
          <TouchableOpacity
            style={styles.addEmployee}
            onPress={OnPressAddToList}>
            <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
              Add To list
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

function connectToStore(store) {
  return {
    loginList: store.cartReducer.loginList,
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D8E3E7',
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  profileImg: {
    // margin: '3%',
    height: 150,
    width: 150,
    borderRadius: 80,
    // marginBottom: 5,
  },
  main: {
    flex: 0.4,
    width: '95%',
    height: '100%',
    // backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#629d88',
  },
  input: {
    // width:'100%',
    // height: 45,
    // margin: 10,
    borderWidth: 0.5,
    padding: 10,
    // backgroundColor: 'grey',
    borderRadius: 10,
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 13,
  },
  textInput: {
    marginTop: -15,
    fontSize: 13,
    // color: 'white',
    borderBottomWidth: 0.5,
    width: 300,
    borderBottomColor: 'white',
  },
  text_main: {
    fontSize: 15,
    color: 'white',
    marginTop: 5,
  },
  input: {
    flexDirection: 'row',
    marginTop: 5,
  },
  addEmployee: {
    width: '60%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: 'white',
    borderWidth: 3,
    // marginBottom: 15,
    paddingRight: 20,
    paddingLeft: 20,
  },
  button: {
    alignItems: 'center',
    marginTop: '20%',
  },
});
