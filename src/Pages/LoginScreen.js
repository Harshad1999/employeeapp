import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';
import {empty, isArrEmpty} from '../utils/Validate';
import {getUser, setUser} from '../utils/Storage';
import {useNavigation} from '@react-navigation/native';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import * as cartAction from '../redux/action/CartAction';

export default function LoginScreen() {
  const [showPassword, setShowPassword] = useState(true);
  const [inputUser, setinputUser] = useState('');
  const [inputPass, setinputPass] = useState('');
  const [errorUser, seterrorUser] = useState('');
  const [errorPass, seterrorPass] = useState('');

  let store = useSelector(connectToStore, shallowEqual);
  let dispatch = useDispatch();

  useEffect(() => {
    sessionCheck();
  }, []);

  useEffect(() => {
    let {loginList} = store;

    if (!isArrEmpty(loginList) && !empty(inputUser)) {
      for (let i = 0; i < loginList.length; i++) {
        let obj = loginList[i];
        if (inputUser === obj.username && inputPass === obj.password) {
          setUser(obj);
          clearValue();
          sessionCheck();
          return;
        }
      }
      showToastError();
    }
  }, [store.loginList]);

  async function sessionCheck() {
    let getuserData = await getUser();
    if (!empty(getuserData.password)) {
      navigation.navigate('Drawer');
    }
  }

  let navigation = useNavigation();

  function OnChangeUserText(text) {
    setinputUser(text);
  }
  function OnChangePassText(text) {
    setinputPass(text);
  }

  const showToastError = () => {
    ToastAndroid.show('Invalid User', ToastAndroid.SHORT);
  };

  async function OnCLickForgot() {
    let getusername = await getUser();
    setinputUser(getusername.username);
  }

  function clearValue() {
    seterrorUser('');
    seterrorPass('');
    setinputUser('');
    setinputPass('');
  }

  function onPressLogin() {
    let isValidUser = isValid();
    if (isValidUser) {
      let newData = {
        id: Math.random(),
        username: inputUser,
        password: inputPass,
      };

      dispatch(cartAction.getUserLogin(newData));

      // setUser(newData);
      // clearValue();
      // navigation.navigate('Drawer');
    }
  }
  function isValid() {
    let valid = true;
    let errorUserMsg = '';
    let errorPassMsg = '';

    if (empty(inputUser)) {
      valid = false;
      errorUserMsg = 'Enter Username';
    }
    if (empty(inputPass)) {
      valid = false;
      errorPassMsg = 'Enter Password';
    }

    // if (!empty(inputUser) && inputUser !== loginList.username) {
    //   valid = false;
    //   errorUserMsg = 'Invalid Username';
    // }
    // if (!empty(inputPass) && inputPass !== loginList.password) {
    //   valid = false;
    //   errorPassMsg = 'Invalid Password';
    // }
    seterrorUser(errorUserMsg);
    seterrorPass(errorPassMsg);

    return valid;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#77ACF1" />
      <View style={styles.header}>
        <Text style={styles.text_header}>Login!</Text>
      </View>
      <View style={styles.main}>
        <Text style={styles.text_main}>Username</Text>
        <View style={styles.input}>
          <Feather name="user" color="white" size={20} />
          <TextInput
            style={styles.textInput}
            placeholder="Enter Username"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={inputUser}
            onChangeText={OnChangeUserText}
          />
        </View>

        {!empty(errorUser) && (
          <View>
            <Text style={styles.errorMsg}>{errorUser}</Text>
          </View>
        )}

        <Text style={styles.text_main}>Password</Text>
        <View style={styles.input}>
          <Feather name="lock" color="white" size={20} />
          <TextInput
            style={styles.textInput}
            secureTextEntry={showPassword}
            placeholder="Enter Password"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={inputPass}
            onChangeText={OnChangePassText}
          />
          <TouchableOpacity onPress={() => setShowPassword(!showPassword)}>
            {!showPassword && <Feather name="eye-off" color="grey" size={20} />}

            {showPassword && <Feather name="eye" color="grey" size={20} />}
          </TouchableOpacity>
        </View>

        {!empty(errorPass) && (
          <View>
            <Text style={styles.errorMsg}>{errorPass}</Text>
          </View>
        )}

        <TouchableOpacity onPress={OnCLickForgot}>
          <Text style={{color: '#77ACF1', marginTop: 17, fontSize: 17}}>
            Forgot password?
          </Text>
        </TouchableOpacity>
        <View style={styles.button}>
          <TouchableOpacity style={styles.signInBtn} onPress={onPressLogin}>
            <Text style={styles.textSignIn}>Sign In</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#77ACF1',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  main: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
    backgroundColor: '#424242',
  },
  text_header: {
    color: '#542E71',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_main: {
    fontSize: 18,
    color: 'white',
    marginTop: 30,
  },
  input: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    paddingBottom: 5,
  },

  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 10,
    color: 'white',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 15,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signInBtn: {
    width: '60%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#77ACF1',
    borderWidth: 3,
    marginTop: 15,
  },
  textSignIn: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#77ACF1',
  },
});

function connectToStore(store) {
  return {
    loginList: store.cartReducer.loginList,
  };
}
