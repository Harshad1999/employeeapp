import {
  View,
  TextInput,
  Text,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
// import {getEmpData} from '../utils/EmployeeData';
import Emp_Card from '../components/Emp_Card';
import {useNavigation} from '@react-navigation/native';
import Feather from 'react-native-vector-icons/dist/Feather';
import {FlatList} from 'react-native-gesture-handler';
import {empty, isArrEmpty} from '../utils/Validate';
import _ from 'lodash';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import * as cartAction from '../redux/action/CartAction';

export default function MainHome() {
  const [empData, setEmpData] = useState([]);
  // const [filteredData, setfilteredData] = useState([]);
  const [search, setsearch] = useState('');

  const [action, setAction] = useState(false);
  const [filter, setFilter] = useState('name');

  let navigation = useNavigation();
  let store = useSelector(connectToStore, shallowEqual);
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(cartAction.getUserLogin());
  }, []);

  // useEffect(() => {
  //   const unsubscribe = navigation.addListener('focus', () => {
  //     async function gettingData() {
  //       // let employeeData = await getEmp('empKey');
  //       let employeeData = await store.EmpList;
  //       if (!isArrEmpty(employeeData)) {
  //         setEmpData(employeeData);
  //       }
  //     }
  //     gettingData();
  //   });

  //   return () => {
  //     unsubscribe;
  //   };
  // }, [navigation]);

  // useEffect(() => {
  //   let {isDeleted} = store;
  //   if (isDeleted !== null) {
  //     dispatch(cartAction.getUserLogin());
  //   }
  // }, [store.isDeleted]);

  const myIcon = <Feather name="more-vertical" size={25} color="black" />;

  // const nameSearch = 'name';

  // const teamSearch = 'team';

  function renderData() {
    // if (isArrEmpty(empData)) {
    //   return null;
    // }

    let newData = _.cloneDeep(store.loginList);
    if (!empty(search)) {
      newData = store.loginList.filter(item => {
        if (filter === 'name') {
          const itemData = item.name
            ? item.name.toUpperCase()
            : ''.toUpperCase();
          const textData = search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        }
        if (filter === 'role') {
          const itemData = item.role
            ? item.role.toUpperCase()
            : ''.toUpperCase();
          const textData = search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        }
      });
    }
    return (
      <FlatList
        data={newData}
        keyExtractor={item => item.id}
        renderItem={({item, index}) => {
          return (
            <Emp_Card
              id={!empty(item.id) ? item.id : 'No Id'}
              name={!empty(item.name) ? item.name : 'No Name'}
              // team={!empty(item.team) ? item.team : 'No Team'}
              role={!empty(item.role) ? item.role : 'No Role'}
              profImage={item.profImage}
              // bio={item.bio}
              OnPressTrash={() => removeObj(item, index)}
              onPressEdit={() =>
                navigation.navigate(
                  'AddEmployee',
                  {
                    itemVal: item,
                    indexVal: index,
                  },
                  dispatch(cartAction.putUserLogin(item.id)),
                )
              }
              onPressCard={() =>
                navigation.navigate('EmpProfile', {
                  itemVal: item,
                  indexVal: index,
                })
              }
            />
          );
        }}
      />
    );
  }

  function OnchangeSearch(text) {
    setsearch(text);
  }

  function SearchViaTeam() {
    setFilter('role');
    setAction(false);
  }

  function SearchViaName() {
    setFilter('name');
    setAction(false);
  }

  function removeObj(item, index) {
    // let temp = [...empData];
    // temp.splice(index, 1);
    // setEmpData(temp);
    // removeEmp('empKey');
    dispatch(cartAction.delUserLogin(item.id));
  }

  return (
    <View style={{flex: 1, backgroundColor: '#D8E3E7'}}>
      <StatusBar backgroundColor="grey" />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            borderWidth: 1,
            alignItems: 'center',
            flexDirection: 'row',
            padding: 5,
            borderColor: 'black',
            borderRadius: 12,
            height: 50,
            marginEnd: 10,
            marginStart: 10,
            margin: 8,
          }}>
          <Feather
            name="search"
            color="grey"
            size={18}
            style={{marginEnd: 5, marginStart: 5}}
          />
          <TextInput
            style={{
              width: '80%',
              marginRight: 5,
              fontSize: 15,
              color: 'black',
            }}
            placeholder="Search Employee"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={search}
            onChangeText={text => OnchangeSearch(text)}
          />
          <Feather
            name="more-vertical"
            size={25}
            color="black"
            onPress={() => setAction(!action)}
          />
        </View>
      </View>
      {action && (
        <View
          style={{
            width: 130,
            height: 80,
            backgroundColor: '#D8E3E7',
            marginLeft: 220,
            zIndex: 1,
            borderWidth: 2,
            marginTop: 40,
            borderRadius: 10,
            position: 'absolute',
            justifyContent: 'space-evenly',
          }}>
          <TouchableOpacity onPress={SearchViaName}>
            <Text
              style={{
                color: 'black',
                justifyContent: 'center',
                textAlign: 'center',
                fontSize: 18,
              }}>
              Name
            </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={SearchViaTeam}>
            <Text
              style={{
                color: 'black',
                justifyContent: 'center',
                textAlign: 'center',
                fontSize: 18,
              }}>
              Role
            </Text>
          </TouchableOpacity>
        </View>
      )}
      {store.showLoader && <ActivityIndicator size={'large'} />}
      {!store.showLoader && <View style={{flex: 0.9}}>{renderData()}</View>}
    </View>
  );
}
function connectToStore(store) {
  return {
    deleteMsg: store.cartReducer.deleteMsg,
    loginList: store.cartReducer.loginList,
    userList: store.cartReducer.userList,
    showLoader: store.cartReducer.showLoader,
    EmpList: store.empCardReducer.EmpList,
    isDeleted: store.cartReducer.isDeleted,
  };
}
