import {View, Text} from 'react-native';
import React from 'react';

export default function AboutUS() {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
      }}>
      <Text
        style={{
          fontSize: 50,
        }}>
        About Us
      </Text>
    </View>
  );
}
