import {View, TextInput, TouchableOpacity} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';
import {FlatList} from 'react-native-gesture-handler';

export default function SearchEmp() {
  return (
    <View
      style={{
        flex: 1,
        marginTop: 10,
        alignSelf: 'center',
      }}>
      <View
        style={{
          borderWidth: 1,
          alignItems: 'center',
          flexDirection: 'row',
          padding: 5,
          borderColor: 'black',
          borderRadius: 30,
          height: 50,
          marginEnd: 10,
          marginStart: 10,
        }}>
        <Feather name="search" color="grey" size={20} />
        <TextInput
          style={{
            width: '90%',
            marginRight:5,
            fontSize: 15,
            color: 'black',
          }}
          placeholder="Search Employee"
          placeholderTextColor="grey"
          autoCapitalize="none"
          // value={inputPass}
          // onChangeText={OnChangePassText}
        />
      </View>

      <FlatList />
    </View>
  );
}
