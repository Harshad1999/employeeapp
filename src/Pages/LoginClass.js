import {
  Text,
  View,
  TextInput,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import React, {Component} from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';
import {empty, isArrEmpty} from '../utils/Validate';
import {connect} from 'react-redux';
import * as cartAction from '../redux/action/CartAction';
import {getUser, setUser} from '../utils/Storage';

class LoginClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: true,
      inputUser: '',
      inputPass: '',
      errorUser: '',
      errorPass: '',
    };
    this.onPressLogin = this.onPressLogin.bind(this);
    this.OnChangePassText = this.OnChangePassText.bind(this);
    this.OnChangeUserText = this.OnChangeUserText.bind(this);
    this.onPressEye = this.onPressEye.bind(this);
    this.isValid = this.isValid.bind(this);
    this.sessionCheck = this.sessionCheck.bind(this);
    // this.loginFUnction = this.loginFUnction.bind(this);
  }

  onPressLogin() {
    let isValidUser = this.isValid();
    if (isValidUser) {
      let newData = {
        id: Math.random(),
        username: this.state.inputUser,
        password: this.state.inputPass,
      };
      console.log('newdata', newData);

      this.props.dispatch(cartAction.getUserLogin());

      // setUser(newData);
      // clearValue();
      // this.props.navigation.navigate('Drawer');
    }
  }
  componentDidMount() {
    // let {loginList} = store;
    console.log('Login list', this.props.loginList);
    this.sessionCheck();
  }
  componentDidUpdate(prevProps, prevState) {
    // if (this.props.loginList)
    this.loginFUnction();
  }

  loginFUnction() {
    let loginList = this.props.loginList;
    console.log('loginList update', loginList);
    if (!isArrEmpty(loginList) && !empty(this.state.inputUser)) {
      for (let i = 0; i < loginList.length; i++) {
        const element = loginList[i];
        if (
          this.state.inputUser === element.username &&
          this.state.inputPass === element.password
        ) {
          setUser(element);
          this.setState((state, props) => ({
            inputUser: '',
            inputPass: '',
          }));
          this.props.navigation.navigate('Drawer');
          // this.sessionCheck();
          return;
        }
      }
    }
  }

  async sessionCheck() {
    let userData = await getUser();
    console.log('before empty-->', userData);
    if (!empty(userData.password)) {
      console.log('after empty->', userData);
      this.props.navigation.navigate('Drawer');
    }
  }

  OnChangeUserText(text) {
    // setinputUser(text);
    // this.setState({inputUser: text});
    this.setState((state, props) => ({
      inputUser: text,
    }));
    console.log('Username :', text);
  }
  OnChangePassText(text) {
    // setinputPass(text);
    // this.setState({inputPass: text});
    this.setState((state, props) => ({
      inputPass: text,
    }));
    console.log('Password :', text);
  }

  isValid() {
    let valid = true;
    let errorUserMsg = '';
    let errorPassMsg = '';

    if (empty(this.state.inputUser)) {
      valid = false;
      errorUserMsg = 'Enter Username';
    }
    if (empty(this.state.inputPass)) {
      valid = false;
      errorPassMsg = 'Enter Password';
    }
    this.setState((state, props) => ({
      errorUser: errorUserMsg,
    }));
    this.setState((state, props) => ({
      errorPass: errorPassMsg,
    }));

    return valid;
  }

  onPressEye() {
    this.setState(prevState => ({
      showPassword: !prevState.showPassword,
    }));
    console.log(this.state.showPassword);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#629d88" />
        <View style={styles.header}>
          <Text style={styles.text_header}>Login!</Text>
        </View>
        <View style={styles.main}>
          <Text style={styles.text_main}>Username</Text>
          <View style={styles.input}>
            <Feather name="user" color="white" size={20} />
            <TextInput
              style={styles.textInput}
              placeholder="Enter Username"
              placeholderTextColor="grey"
              autoCapitalize="none"
              value={this.state.inputUser}
              onChangeText={this.OnChangeUserText}
            />
          </View>

          {!empty(this.state.errorUser) && (
            <View>
              <Text style={styles.errorMsg}>{this.state.errorUser}</Text>
            </View>
          )}

          <Text style={styles.text_main}>Password</Text>
          <View style={styles.input}>
            <Feather name="lock" color="white" size={20} />
            <TextInput
              style={styles.textInput}
              secureTextEntry={this.state.showPassword}
              placeholder="Enter Password"
              placeholderTextColor="grey"
              autoCapitalize="none"
              value={this.state.inputPass}
              onChangeText={this.OnChangePassText}
            />
            <TouchableOpacity
              onPress={() =>
                this.setState((state, props) => ({
                  showPassword: !this.state.showPassword,
                }))
              }>
              {!this.state.showPassword && (
                <Feather name="eye-off" color="grey" size={20} />
              )}

              {this.state.showPassword && (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>

          {!empty(this.state.errorPass) && (
            <View>
              <Text style={styles.errorMsg}>{this.state.errorPass}</Text>
            </View>
          )}

          {/* <TouchableOpacity onPress={OnCLickForgot}>
            <Text style={{color: '#629d88', marginTop: 17, fontSize: 17}}>
              Forgot password?
            </Text>
          </TouchableOpacity> */}
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.signInBtn}
              onPress={this.onPressLogin}>
              <Text style={styles.textSignIn}>Sign In</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#629d88',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  main: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
    backgroundColor: '#424242',
  },
  text_header: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_main: {
    fontSize: 18,
    color: 'white',
    marginTop: 30,
  },
  input: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    paddingBottom: 5,
  },

  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 10,
    color: 'white',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 15,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signInBtn: {
    width: '60%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#629d88',
    marginTop: 15,
  },
  textSignIn: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
  },
});

const mapStateToProp = store => {
  return {
    loginList: store.cartReducer.loginList,
  };
};

const mapDispatchToProp = dispatch => {
  return {dispatchLoginList: () => dispatch(cartAction.getUserLogin())};
};

export default connect(mapStateToProp, null)(LoginClass);
