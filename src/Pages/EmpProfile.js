import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import * as cartAction from '../redux/action/CartAction';

export default function EmpProfile(props) {
  let store = useSelector(connectToStore, shallowEqual);
  let dispatch = useDispatch();
  const {itemVal, indexVal} = props.route.params;

  function OnPressUpdateToList() {
    showToastSuccess(cartAction.putUserLogin(itemVal.id));
    dispatch(cartAction.putUserLogin(itemVal.id));
  }
  const showToastSuccess = () => {
    ToastAndroid.show('Data Updated Successfully', ToastAndroid.SHORT);
  };

  return (
    <View style={style.container}>
      <View
      // contentContainerStyle={{
      //   flex: 0.9,
      //   backgroundColor: '#77ACF1',
      //   // width: '93%',
      //   alignItems: 'center',
      //   borderBottomEndRadius: 50,
      // }}>
      >
        <Text style={style.nameText}>{itemVal.id}</Text>
        <TouchableHighlight>
          <Image source={{uri: itemVal.profImage}} style={style.profileImg} />
        </TouchableHighlight>
        <View style={{marginTop: 50}}>
          <TextInput style={style.nameText}>{itemVal.name}</TextInput>
          <TextInput style={style.detailText}>{itemVal.role}</TextInput>

          {/* <Text style={[style.detailText, {marginTop: 20}]}>
            {itemVal.team}
          </Text> */}
          {/* <Text style={style.detailText}>{itemVal.bio}</Text> */}
        </View>
        <View style={style.button}>
          <TouchableOpacity
            style={style.addEmployee}
            onPress={OnPressUpdateToList}>
            <Text style={{fontSize: 15, fontWeight: 'bold', color: '#542E71'}}>
              Update To list
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

function connectToStore(store) {
  return {
    loginList: store.cartReducer.loginList,
  };
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D8E3E7',
    alignItems: 'center',
  },

  profileImg: {
    marginTop: '10%',
    height: 200,
    width: 200,
    borderRadius: 30,
  },
  nameText: {
    fontSize: 33,
    color: '#542E71',
    marginTop: 20,
    marginLeft: 20,
    fontWeight: '500',
  },
  detailText: {
    alignItems: 'flex-start',
    fontSize: 18,
    marginLeft: 20,
    paddingRight: 20,
    marginTop: 2,
    color: '#000',
  },
  button: {
    alignItems: 'center',
    marginTop: '20%',
  },
  addEmployee: {
    width: '70%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#542E71',
    borderWidth: 3,
    // marginBottom: 15,
    paddingRight: 10,
    paddingLeft: 10,
  },
});
