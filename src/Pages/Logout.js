import {View, Alert, Text} from 'react-native';
import React, {useEffect} from 'react';
import {removeUser, setUser} from '../utils/Storage';
import {useNavigation} from '@react-navigation/native';

export default function Logout() {
  let navigation = useNavigation();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      onPressLogout();
    });
    return () => {
      unsubscribe;
    };
  }, [navigation]);

  async function clearSession() {
    let rm = await removeUser();
    
    console.log(rm);
    if (rm) {
      navigation.navigate('LoginClass');
    }
  }

  function onPressLogout() {
    Alert.alert('Logout', 'Are you sure', [
      {
        text: 'Cancel',
        onPress: () => navigation.navigate('Drawer'),
        style: 'cancel',
      },
      {
        text: 'Ok',
        onPress: () => {
          clearSession();
          navigation.navigate('Login');
        },
      },
    ]);
  }

  return (
    <View>
      <Text style={{color: 'white'}}>Logout</Text>
    </View>
  );
}
