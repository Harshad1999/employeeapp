import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Card from '../components/Card';
import {
  empty,
  valNum,
  valAlphaNum,
  valAlpha,
  isArrEmpty,
} from '../utils/Validate';

import {getItem, setItem} from '../utils/Storage';

export default function Cart() {
  const [Total, setTotal] = useState(0);

  const [listData, setListData] = useState([]);

  const [prodNameValue, setProdNameValue] = useState({
    value: '',
    error: '',
  });
  const [prodDescValue, setProdDescValue] = useState({
    value: '',
    error: '',
  });
  const [prodPriceValue, setProdPriceValue] = useState({
    value: '',
    error: '',
  });

  let isValid = formValid();

  // useEffect(() => {
  //   let cardData = getCardData();
  //   if (!isArrEmpty(cardData)) {
  //     setListData(cardData);
  //     // console.log('getting data');
  //   }
  // }, []);

  useEffect(() => {
    addAll();
  }),
    [listData];

  function formValid() {
    let valid = true;

    if (empty(prodNameValue.value)) {
      valid = false;
    }
    // if (!valAlphaNum(prodNameValue.value)) {
    //   valid = false;
    // }

    if (empty(prodDescValue.value)) {
      valid = false;
    }
    // if (!valAlpha(prodDescValue.value)) {
    //   valid = false;
    // }
    if (empty(prodPriceValue.value)) {
      valid = false;
    }
    // if (!valNum(prodPriceValue.value)) {
    //   valid = false;
    // }

    return valid;
  }

  function onPressPlus(item, index) {
    let temp = [...listData];
    temp[index].qty = temp[index].qty + 1;
    setListData(temp);
  }

  function onPressMinus(item, index) {
    if (listData[index].qty > 1) {
      let temp = [...listData];
      temp[index].qty = temp[index].qty - 1;
      setListData(temp);
    }
  }
  function addAll() {
    console.log('ListData', listData);
    let tot = 0;
    for (i = 0; i < listData.length; i++) {
      const element = listData[i];
      tot = tot + parseInt(element.price) * element.qty;
    }
    setTotal(tot);
  }

  function clearValue() {
    setProdDescValue('');
    setProdNameValue('');
    setProdPriceValue('');
  }

  function onPressTrash(item, index) {
    let temp = [...listData];

    temp.splice(index, 1);

    setListData(temp);
    setItem('newKey', temp);
  }

  async function onPressGet() {
    let tempArray = _.cloneDeep(listData);
    let cardData = await getItem('newKey');
    tempArray = cardData;
    if (empty(cardData)) {
      return null;
    }

    setListData(tempArray);
  }

  function onPressAdd() {
    if (isValid) {
      let newData = {
        key: Math.random(),
        prodname: prodNameValue.value,
        proddesc: prodDescValue.value,
        price: prodPriceValue.value,
        qty: 1,
      };

      let temp = [...listData];

      temp.push(newData);

      setItem('newKey', temp);

      clearValue();
    } else console.log('Empty data');
  }

  function renderHeaderComponent() {
    return (
      <View style={style.inpviewtwo}>
        <View style={{width: '50%'}}>
          <TextInput
            style={{
              height: 45,
              margin: 10,
              borderWidth: 0.5,
              padding: 10,
              backgroundColor: 'grey',
              borderRadius: 10,
            }}
            placeholder="Enter Desc"
            keyboardType="ascii-capable"
            onChangeText={onChangeDescValue}
            value={prodDescValue.value}
          />
          {!empty(prodDescValue.error) && (
            <Text style={{color: 'red', fontSize: 20, marginLeft: 10}}>
              {prodDescValue.error}
            </Text>
          )}
        </View>

        <View style={{width: '50%'}}>
          <TextInput
            style={{
              height: 45,
              margin: 10,
              borderWidth: 0.5,
              padding: 10,
              backgroundColor: 'grey',
              borderRadius: 10,
            }}
            placeholder="Enter Price"
            keyboardType="number-pad"
            onChangeText={onChangePriceValue}
            value={prodPriceValue.value}
          />
          {!empty(prodPriceValue.error) && (
            <Text style={{color: 'red', fontSize: 20, marginLeft: 10}}>
              {prodPriceValue.error}
            </Text>
          )}
        </View>
      </View>
    );
  }

  function renderSeparator() {
    return <View style={style.seperatorStyle} />;
  }

  function renderCardData() {
    if (isArrEmpty(listData)) {
      return null;
    }
    // let newListData = listData ? (Array.isArray(listData) ? listData : []) : [];
    return (
      <FlatList
        data={listData}
        keyExtractor={item => item.key}
        // ListHeaderComponent={renderHeaderComponent()}
        ItemSeparatorComponent={renderSeparator}
        // numColumns={2}
        renderItem={({item, index}) => {
          return (
            <Card
              // qtyValue={qtyValue}
              onPressPlus={() => onPressPlus(item, index)}
              onPressMinus={() => onPressMinus(item, index)}
              prodname={!empty(item.prodname) ? item.prodname : 'NA'}
              proddesc={!empty(item.proddesc) ? item.proddesc : 'NA'}
              price={!empty(item.price) ? item.price : 0}
              qty={!empty(item.qty) ? item.qty : 1}
              // imageuri={item.imageuri}
              onPressTrash={() => onPressTrash(item, index)}
              imageuri="https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-13-pro-blue-select?wid=470&hei=556&fmt=png-alpha&.v=1631652954000"
            />
          );
        }}
      />
    );
  }

  function onChangeNameValue(v) {
    let error = '';
    let value = v;
    if (!valAlphaNum(v)) {
      error = 'Enter valid Product name';
      value = '';
    }
    setProdNameValue({...prodNameValue, value: value, error: error});
  }
  function onChangeDescValue(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid description';
      value = '';
    }
    setProdDescValue({...prodDescValue, value: value, error: error});
  }
  function onChangePriceValue(v) {
    let error = '';
    let value = v;
    if (!valNum(v)) {
      error = 'Enter valid Amount';
      value = '';
    }
    setProdPriceValue({...prodPriceValue, value: value, error: error});
  }

  return (
    <View style={style.container}>
      <TextInput
        style={style.input}
        placeholder="Enter Product Name"
        keyboardType="ascii-capable"
        onChangeText={onChangeNameValue}
        value={prodNameValue.value}
      />
      {!empty(prodNameValue.error) && (
        <Text style={{color: 'red', fontSize: 20, marginLeft: 10}}>
          {prodNameValue.error}
        </Text>
      )}
      {renderHeaderComponent()}
      <View style={style.scroll}>{renderCardData()}</View>
      <View style={style.foot}>
        <Text style={style.totalprice}>₹ {Total}</Text>
        <TouchableOpacity style={style.button} onPress={onPressGet}>
          <Text style={{padding: 2, fontSize: 19, color: 'white'}}>get</Text>
        </TouchableOpacity>
        <TouchableOpacity style={style.button} onPress={onPressAdd}>
          <Text style={{padding: 2, fontSize: 19, color: 'white'}}>
            Add To List
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#424242',
  },
  seperatorStyle: {
    height: 1,
    width: '100%',
    backgroundColor: '#CFDBFC',
    margin: 2,
  },
  button: {
    backgroundColor: '#6466f1',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    marginEnd: 25,
    margin: 8,
    height: 40,
    width: 120,
  },
  input: {
    height: 45,
    margin: 10,
    borderWidth: 0.5,
    padding: 10,
    backgroundColor: 'grey',
    borderRadius: 10,
  },
  foot: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#424242',
  },
  inpviewtwo: {
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#424242',
  },
  scroll: {
    flex: 1,
    height: 50,
  },
  totalprice: {
    fontSize: 30,
    color: 'white',
    alignContent: 'center',
    margin: 5,
    marginStart: 15,

    fontFamily: 'GideonRoman-Regular',
  },
  headertxt: {
    fontSize: 50,
    color: 'white',
    margin: 10,
    fontFamily: 'SyneTactile-Regular',
  },
  pagetext: {
    fontSize: 20,
    color: 'white',
    marginStart: 15,
    marginBottom: 10,
  },
});
