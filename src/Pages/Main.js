import {View, Text} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';
import * as counterAction from '../redux/action/CounterAction';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';

export default function Main() {
  let store = useSelector(connectToStore, shallowEqual);
  let dispatch = useDispatch();
  function OnIncrement() {
    dispatch(counterAction.Increment());
  }

  function OnDecrement() {
    dispatch(counterAction.Decrement());
  }
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
      }}>
      <Feather
        name="plus"
        size={50}
        style={{marginBottom: 20}}
        onPress={OnIncrement}
      />
      <Text
        style={{
          fontSize: 50,
        }}>
        {store.count}
      </Text>
      <Feather
        name="minus"
        size={50}
        style={{marginTop: 20}}
        onPress={OnDecrement}
      />
    </View>
  );

  function connectToStore(store) {
    return {
      count: store.counterReducer.count,
    };
  }
}


