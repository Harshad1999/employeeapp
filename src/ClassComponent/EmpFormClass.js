import {
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';
import React, {Component} from 'react';
import {
  empty,
  isArrEmpty,
  valAlpha,
  valAlphaNum,
  valNum,
} from '../utils/Validate';
import Feather from 'react-native-vector-icons/dist/Feather';
import ImagePicker from 'react-native-image-crop-picker';
import {connect} from 'react-redux';
import * as cartAction from '../redux/action/CartAction';

export class EmpFormClass extends Component {
  constructor(props) {
    super(props);

    this.state = {
      image: '',
      empUsernameValue: '',
      empUsernameError: '',
      empPasswordValue: '',
      empPasswordError: '',
      empNameValue: '',
      empNameError: '',
      empTeamValue: '',
      empTeamErrorL: '',
      empRoleValue: '',
      empRoleError: '',
    };
    this.OpenCamera = this.OpenCamera.bind(this);
    this.pickImage = this.pickImage.bind(this);
    this.OnChangeUsernameValue = this.OnChangeUsernameValue.bind(this);
    this.OnChangePasswordValue = this.OnChangePasswordValue.bind(this);
    this.OnChangeNameText = this.OnChangeNameText.bind(this);
    this.OnChangeTeamValue = this.OnChangeTeamValue.bind(this);
    this.OnChangeRoleText = this.OnChangeRoleText.bind(this);
    this.formValid = this.formValid.bind(this);
    this.OnPressAddToList = this.OnPressAddToList.bind(this);
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      // do something
      console.log('Sankalp');
      const itemVal =
        this.props.route.params !== undefined &&
        this.props.route.params.itemVal !== undefined
          ? this.props.route.params.itemVal
          : undefined;
      console.log(itemVal);
      this.setState(() => ({
        empUsernameValue: itemVal !== undefined ? itemVal.username : '',
        empPasswordValue: itemVal !== undefined ? itemVal.password : '',
      }));
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
    console.log('saxena');
  }

  clearValues() {
    this.setState((state, props) => ({
      empUsernameValue: '',
      empPasswordValue: '',
      empNameValue: '',
      empTeamValue: '',
      empRoleValue: '',
      image: '',
    }));
  }

  formValid() {
    let valid = true;
    let errorMsg = '';

    if (empty(this.state.empUsernameValue)) {
      valid = false;
      errorMsg = 'Enter Username';
      this.setState((state, props) => ({
        empUsernameError: errorMsg,
      }));
    }
    if (empty(this.state.empPasswordValue)) {
      valid = false;
      errorMsg = 'Enter Password';
      this.setState((state, props) => ({
        empPasswordError: errorMsg,
      }));
    }
    if (empty(this.state.empNameValue)) {
      valid = false;
      errorMsg = 'Enter Name';
      this.setState((state, props) => ({
        empNameError: errorMsg,
      }));
    }
    if (empty(this.state.empTeamValue)) {
      valid = false;
      errorMsg = 'Enter Team';
      this.setState((state, props) => ({
        empTeamError: errorMsg,
      }));
    }
    if (empty(this.state.empRoleValue)) {
      valid = false;
      errorMsg = 'Enter Role';
      this.setState((state, props) => ({
        empRoleError: errorMsg,
      }));
    }
    return valid;
  }

  OnChangeUsernameValue(v) {
    let error = '';
    let value = v;
    if (!valAlphaNum(v)) {
      error = 'Enter valid Username';
      value = '';
    }
    this.setState((state, props) => ({
      empUsernameValue: value,
      empUsernameError: error,
    }));
  }
  OnChangePasswordValue(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Password';
      value = '';
    }
    this.setState((state, props) => ({
      empPasswordValue: value,
      empPasswordError: error,
    }));
  }
  OnChangeNameText(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Name';
      value = '';
    }
    this.setState((state, props) => ({
      empNameValue: value,
      empNameError: error,
    }));
  }
  OnChangeTeamValue(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Team';
      value = '';
    }
    this.setState((state, props) => ({
      empTeamValue: value,
      empTeamError: error,
    }));
  }
  OnChangeRoleText(v) {
    let error = '';
    let value = v;
    if (!valAlpha(v)) {
      error = 'Enter valid Role';
      value = '';
    }
    this.setState((state, props) => ({
      empRoleValue: value,
      empRoleError: error,
    }));
  }

  async OnPressAddToList() {
    let isValid = this.formValid();
    if (isValid) {
      let newEmpData = {
        username: this.state.empUsernameValue,
        password: this.state.empPasswordValue,
        name: this.state.empNameValue,
        team: this.state.empTeamValue,
        role: this.state.empRoleValue,
        profImage: this.state.image
          ? this.state.image
          : 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png',
      };

      if (!isArrEmpty(newEmpData)) {
        console.log('newdata', newEmpData);
      }
      this.clearValues();
      this.props.dispatch(cartAction.setUserLogin(newEmpData));
      this.props.navigation.navigate('EmployeesList');

      // let oldData = await getEmp('empKey');
      // let oldData = store.EmpList;
      // console.log(oldData);

      // // let temp = [...empData];
      // if (empty(oldData)) {
      //   oldData = [];
      // }
      // oldData.push(newEmpData);
      // // setEmpData(temp);
      // setEmp('empKey', oldData);
      // clearValue();

      //   showToastSuccess(cartAction.setUserLogin(newEmpData));
      // dispatch(cartAction.setUserLogin(newEmpData));
      //   console.log(store.loginList);
      // showToastSuccess(cartReducer.setUserLogin(newEmpData));
      // get
      // console.log('onpress add', temp);
      // } else showToastError()
    }
  }

  async OpenCamera() {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      this.setState((state, props) => ({
        image: image.path,
      }));
      //   setImage(image.path);
    });
  }

  async pickImage() {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      this.setState((state, props) => ({
        image: image.path,
      }));
      //   setImage(image.path);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{flex: 0.4, alignItems: 'center'}}>
          <Image
            source={{
              uri: this.state.image
                ? this.state.image
                : 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png',
            }}
            style={styles.profileImg}
          />
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => {
                this.OpenCamera();
              }}>
              <Feather
                name="camera"
                size={20}
                style={{color: 'black', marginRight: 100}}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.pickImage();
              }}>
              <Feather name="image" size={20} style={{color: 'black'}} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={styles.main}>
          <Text style={styles.text_main}>Username : </Text>
          <View style={styles.input}>
            <View>
              <TextInput
                style={styles.textInput}
                placeholder="Harshadxyz"
                placeholderTextColor="#D0D0D0"
                // keyboardType="numb"
                color="white"
                value={this.state.empUsernameValue}
                onChangeText={this.OnChangeUsernameValue}
              />
              {!empty(this.state.empUsernameError) && (
                <Text style={styles.errorMsg}>
                  {this.state.empUsernameError}
                </Text>
              )}
            </View>
          </View>
          <Text style={styles.text_main}>Password : </Text>
          <View style={styles.input}>
            <View>
              <TextInput
                style={styles.textInput}
                placeholder="eg: John"
                color="white"
                placeholderTextColor="#D0D0D0"
                autoCapitalize="none"
                // keyboardType="number-pad"
                value={this.state.empPasswordValue}
                onChangeText={this.OnChangePasswordValue}
              />
              {!empty(this.state.empPasswordError) && (
                <Text style={styles.errorMsg}>
                  {this.state.empPasswordError}
                </Text>
              )}
            </View>
          </View>
          <Text style={styles.text_main}>Employee Name : </Text>
          <View style={styles.input}>
            <View>
              <TextInput
                style={styles.textInput}
                placeholder="eg: John"
                placeholderTextColor="#D0D0D0"
                autoCapitalize="none"
                color="white"
                value={this.state.empNameValue}
                onChangeText={this.OnChangeNameText}
              />
              {!empty(this.state.empNameError) && (
                <Text style={styles.errorMsg}>{this.state.empNameError}</Text>
              )}
            </View>
          </View>
          <Text style={styles.text_main}>Team: </Text>
          <View style={styles.input}>
            <View>
              <TextInput
                style={styles.textInput}
                placeholder="eg: React Native"
                placeholderTextColor="#D0D0D0"
                autoCapitalize="none"
                color="white"
                value={this.state.empTeamValue}
                onChangeText={this.OnChangeTeamValue}
              />
              {!empty(this.state.empTeamError) && (
                <Text style={styles.errorMsg}>{this.state.empTeamError}</Text>
              )}
            </View>
          </View>
          <Text style={styles.text_main}>Role : </Text>
          <View style={styles.input}>
            <View>
              <TextInput
                style={styles.textInput}
                placeholder="eg: admin"
                placeholderTextColor="#D0D0D0"
                autoCapitalize="none"
                color="white"
                value={this.state.empRoleValue}
                onChangeText={this.OnChangeRoleText}
              />
              {!empty(this.state.empRoleError) && (
                <Text style={styles.errorMsg}>{this.state.empRoleError}</Text>
              )}
            </View>
          </View>
          {/* <Text style={styles.text_main}>Bio : </Text>
        <View style={styles.input}>
          <View>
            <TextInput
              multiline
              style={styles.textInput}
              placeholder="eg: Software Developer"
              placeholderTextColor="#D0D0D0"
              autoCapitalize="none"
              color='white'
              value={empBioValue.value}
              onChangeText={OnChangeBioText}
            />
            {!empty(empBioValue.error) && (
              <Text style={styles.errorMsg}>{empBioValue.error}</Text>
            )}
          </View>
        </View> */}
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.addEmployee}
              onPress={this.OnPressAddToList}>
              <Text style={{fontSize: 20, fontWeight: '900', color: '#629d88'}}>
                Add To list
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D8E3E7',
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  profileImg: {
    // margin: '3%',
    height: 150,
    width: 150,
    borderRadius: 80,
    // marginBottom: 5,
  },
  main: {
    flex: 0.4,
    width: '95%',
    height: '100%',
    // backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#629d88',
  },
  input: {
    // width:'100%',
    // height: 45,
    // margin: 10,
    borderWidth: 0.5,
    padding: 10,
    // backgroundColor: 'grey',
    borderRadius: 10,
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 13,
  },
  textInput: {
    marginTop: -15,
    fontSize: 13,
    // color: 'white',
    borderBottomWidth: 0.5,
    width: 300,
    borderBottomColor: 'white',
  },
  text_main: {
    fontSize: 15,
    color: 'white',
    fontWeight: 'bold',
    marginTop: 10,
  },
  input: {
    flexDirection: 'row',
    marginTop: 5,
  },
  addEmployee: {
    width: '60%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    // marginBottom: 15,
    paddingRight: 20,
    paddingLeft: 20,
  },
  button: {
    alignItems: 'center',
    marginTop: '20%',
  },
});

const mapStateToProp = store => {
  return {
    loginList: store.cartReducer.loginList,
  };
};

export default connect(mapStateToProp, null)(EmpFormClass);
