import {
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native';
import React, {Component} from 'react';
import Feather from 'react-native-vector-icons/dist/Feather';
import {empty} from '../utils/Validate';
import {connect} from 'react-redux';
import * as cartAction from '../redux/action/CartAction';
import EmpCardClass from './EmpCardClass';

export class EmpListClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      action: false,
      filter: 'name',
    };

    this.renderData = this.renderData.bind(this);
    this.OnchangeSearch = this.OnchangeSearch.bind(this);
    this.SearchViaTeam = this.SearchViaTeam.bind(this);
    this.SearchViaName = this.SearchViaName.bind(this);
    this.removeObj = this.removeObj.bind(this);
  }
  componentDidMount() {
    this.props.dispatch(cartAction.getUserLogin());
  }
  // componentDidUpdate(prevProps, prevState) {
  //   this.props.dispatch(cartAction.getUserLogin());
  // }

  removeObj(item, index) {
    // let temp = [...empData];
    // temp.splice(index, 1);
    // setEmpData(temp);
    // removeEmp('empKey');
    this.props.dispatch(cartAction.delUserLogin(item.id));
  }

  renderData() {
    // if (isArrEmpty(empData)) {
    //   return null;
    // }

    let newData = [...this.props.loginList];
    if (!empty(this.state.search)) {
      newData = this.props.loginList.filter(item => {
        if (this.state.filter === 'name') {
          const itemData = item.name
            ? item.name.toUpperCase()
            : ''.toUpperCase();
          const textData = this.state.search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        }
        if (this.state.filter === 'role') {
          const itemData = item.role
            ? item.role.toUpperCase()
            : ''.toUpperCase();
          const textData = this.state.search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        }
      });
    }
    return (
      <FlatList
        data={newData}
        keyExtractor={item => item.id}
        renderItem={({item, index}) => {
          return (
            <EmpCardClass
              id={!empty(item.id) ? item.id : 'No Id'}
              name={!empty(item.name) ? item.name : 'No Name'}
              // team={!empty(item.team) ? item.team : 'No Team'}
              role={!empty(item.role) ? item.role : 'No Role'}
              profImage={item.profImage}
              // bio={item.bio}
              OnPressTrash={() => this.removeObj(item, index)}
              onPressEdit={() =>
                this.props.navigation.navigate(
                  'EmployeesForm',
                  {
                    itemVal: item,
                    indexVal: index,
                  },
                  // this.props.dispatch(cartAction.putUserLogin(item.id)),
                )
              }
              onPressCard={() =>
                this.props.navigation.navigate('EmpDetailClass', {
                  itemVal: item,
                  indexVal: index,
                })
              }
            />
          );
        }}
      />
    );
  }

  OnchangeSearch(text) {
    this.setState((state, props) => ({
      search: text,
    }));
  }

  SearchViaTeam() {
    this.setState((state, props) => ({
      filter: 'role',
      action: false,
    }));
  }

  SearchViaName() {
    this.setState((state, props) => ({
      filter: 'name',
      action: false,
    }));
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <StatusBar backgroundColor="grey" />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={{
              borderWidth: 1,
              alignItems: 'center',
              flexDirection: 'row',
              padding: 5,
              borderColor: 'black',
              borderRadius: 12,
              height: 50,
              marginEnd: 10,
              marginStart: 10,
              margin: 8,
            }}>
            <Feather
              name="search"
              color="grey"
              size={18}
              style={{marginEnd: 5, marginStart: 5}}
            />
            <TextInput
              style={{
                width: '80%',
                marginRight: 5,
                fontSize: 15,
                color: 'black',
              }}
              placeholder="Search Employee"
              placeholderTextColor="grey"
              autoCapitalize="none"
              value={this.state.search}
              onChangeText={text => this.OnchangeSearch(text)}
            />
            <Feather
              name="more-vertical"
              size={25}
              color="black"
              onPress={() =>
                this.setState(prevState => ({
                  action: !prevState.action,
                }))
              }
            />
          </View>
        </View>
        {this.state.action && (
          <View
            style={{
              width: 130,
              height: 80,
              backgroundColor: '#fff',
              marginLeft: 220,
              zIndex: 1,
              borderWidth: 2,
              marginTop: 40,
              borderRadius: 10,
              position: 'absolute',
              justifyContent: 'space-evenly',
            }}>
            <TouchableOpacity onPress={this.SearchViaName}>
              <Text
                style={{
                  color: 'black',
                  justifyContent: 'center',
                  textAlign: 'center',
                  fontSize: 18,
                }}>
                Name
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.SearchViaTeam}>
              <Text
                style={{
                  color: 'black',
                  justifyContent: 'center',
                  textAlign: 'center',
                  fontSize: 18,
                }}>
                Role
              </Text>
            </TouchableOpacity>
          </View>
        )}
        <View style={{flex: 0.9}}>{this.renderData()}</View>
      </View>
    );
  }
}

const mapStateToProp = store => {
  return {
    loginList: store.cartReducer.loginList,
  };
};

export default connect(mapStateToProp, null)(EmpListClass);
