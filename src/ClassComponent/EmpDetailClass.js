import {
  Text,
  TextInput,
  Image,
  View,
  TouchableHighlight,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import React, {Component} from 'react';
import { connect } from 'react-redux';

export class EmpDetailClass extends Component {
  constructor(props) {
    super(props);
    this.OnPressUpdateToList = this.OnPressUpdateToList.bind(this);


  }

  OnPressUpdateToList() {
    // dispatch(cartAction.putUserLogin(itemVal.id));
  }

  componentDidMount() {
    console.log('Hi I am here');
  }

  render() {
    const {itemVal, indexVal} = this.props.route.params;
    console.log('item', itemVal);

    return (
      <View style={style.container}>
        <View
        // contentContainerStyle={{
        //   flex: 0.9,
        //   backgroundColor: '#77ACF1',
        //   // width: '93%',
        //   alignItems: 'center',
        //   borderBottomEndRadius: 50,
        // }}>
        >
          <Text style={style.nameText}>{itemVal.id}</Text>
          <TouchableHighlight>
            <Image source={{uri: itemVal.profImage}} style={style.profileImg} />
          </TouchableHighlight>
          <View style={{marginTop: 50}}>
            <TextInput style={style.nameText}>{itemVal.name}</TextInput>
            <TextInput style={style.detailText}>{itemVal.role}</TextInput>

            {/* <Text style={[style.detailText, {marginTop: 20}]}>
            {itemVal.team}
          </Text> */}
            {/* <Text style={style.detailText}>{itemVal.bio}</Text> */}
          </View>
          <View style={style.button}>
            <TouchableOpacity
              style={style.addEmployee}
              onPress={this.OnPressUpdateToList}>
              <Text
                style={{fontSize: 15, fontWeight: 'bold', color: '#542E71'}}>
                Update To list
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D8E3E7',
    alignItems: 'center',
  },

  profileImg: {
    marginTop: '10%',
    height: 200,
    width: 200,
    borderRadius: 30,
  },
  nameText: {
    fontSize: 33,
    color: '#542E71',
    marginTop: 20,
    marginLeft: 20,
    fontWeight: '500',
  },
  detailText: {
    alignItems: 'flex-start',
    fontSize: 18,
    marginLeft: 20,
    paddingRight: 20,
    marginTop: 2,
    color: '#000',
  },
  button: {
    alignItems: 'center',
    marginTop: '20%',
  },
  addEmployee: {
    width: '70%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#542E71',
    borderWidth: 3,
    // marginBottom: 15,
    paddingRight: 10,
    paddingLeft: 10,
  },
});


const mapStateToProp = store => {
  return {
    loginList: store.cartReducer.loginList,
  };
};

export default connect(mapStateToProp, null)(EmpDetailClass);
